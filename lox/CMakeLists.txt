cmake_minimum_required(VERSION 3.25)
project(cpplox_root)

set(CMAKE_CXX_STANDARD 23)

include_directories(src)
add_subdirectory(src)
add_subdirectory(test)
