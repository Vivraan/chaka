//
// Created by vivraan on 05-01-2025.
//

#ifndef EXPR_H
#define EXPR_H
#include <memory>

#include "token.h"

namespace lox::expr {
using token::Token;

struct Expr;
using ExprOwnPtr = std::unique_ptr<const Expr>;

struct GroupingExpr final {
  ExprOwnPtr inner;
};

struct UnaryOpExpr final {
  Token op;
  ExprOwnPtr r;
};

struct BinaryOpExpr final {
  Token op;
  ExprOwnPtr l;
  ExprOwnPtr r;
};

struct TernaryOpExpr final {
  ExprOwnPtr cond;
  ExprOwnPtr t;
  ExprOwnPtr f;
};

struct LiteralExpr final {
  Token k;
};

struct Expr final : std::variant<
      GroupingExpr, UnaryOpExpr, BinaryOpExpr, TernaryOpExpr, LiteralExpr> {
};

ExprOwnPtr make_group(ExprOwnPtr inner);
ExprOwnPtr make_unop(const Token& op, ExprOwnPtr r);
ExprOwnPtr make_binop(const Token& op, ExprOwnPtr l, ExprOwnPtr r);
ExprOwnPtr make_tern(ExprOwnPtr cond, ExprOwnPtr t, ExprOwnPtr f);
ExprOwnPtr make_lit(const Token& k);

std::string parens(std::string_view name,
                   std::initializer_list<Expr*> exprs) noexcept;

std::string to_string(const Expr* expr_tree) noexcept;
} // namespace lox::expr

#endif  // EXPR_H