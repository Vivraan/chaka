#include "runtime.h"

#include <bitset>
#include <filesystem>
#include <fstream>
#include <iostream>

#include "log_err.h"
#include "parser.h"
#include "scanner.h"

namespace lox::runtime {
std::bitset<ErrorKind::_size() + 1> had_error;

void run_file(const std::string_view path) {
  std::ifstream file{path.data()};
  const std::string source{std::istreambuf_iterator{file}, {}};
  run(source);
  if (had_error[ErrorKind::Static]) {
    std::exit(65);
  }
  if (had_error[ErrorKind::Runtime]) {
    std::exit(70);
  }
}

void run_prompt() {
  auto allowed_empty_prompts = 1;
  while (true) {
    std::print("@@@>> ");
    std::string line;
    std::getline(std::cin, line);

    if (line.empty()) {
      allowed_empty_prompts--;
      if (allowed_empty_prompts <= 0) {
        break;
      }
    } else {
      allowed_empty_prompts = 1;
    }
    run(line);
    had_error.reset();
  }
}

void run(const std::string_view source) {
  using parser::Parser;
  using scanner::Scanner;
  using namespace expr;

  const auto tokens = Scanner{source}.scan_tokens();
  const Expr* expr_tree = Parser{tokens}.parse_syntax();

  if (had_error[ErrorKind::Any]) return;

  interpreter.interpret(expr_tree);
}
} // namespace lox::run