//
// Created by vivraan on 01-01-2025.
//

#include "scanner.h"
#include <cctype>
#include <charconv>
#include <span>
#include <system_error>

#include "log_err.h"
#include "token.h"

namespace lox::scanner {
std::span<Token> Scanner::scan_tokens() noexcept {
  while (!is_at_end()) {
    start = current;
    scan_token();
  }
  tokens.push_back(Token{TokenType::Eof, "", LiteralNil, meta});
  return tokens;
}

bool my_isalpha(char c);
bool my_isalnum(char c);

void Scanner::scan_token() noexcept {
  switch (const char c = next()) {
    case '(':
      push_token(TokenType::LParen);
      break;
    case ')':
      push_token(TokenType::RParen);
      break;
    case '{':
      push_token(TokenType::LBrace);
      break;
    case '}':
      push_token(TokenType::RBrace);
      break;
    case ':':
      push_token(TokenType::Colon);
      break;
    case ',':
      push_token(TokenType::Comma);
      break;
    case '.':
      push_token(TokenType::Dot);
      break;
    case '-':
      push_token(TokenType::Minus);
      break;
    case '+':
      push_token(TokenType::Plus);
      break;
    case '?':
      push_token(TokenType::Question);
      break;
    case ';':
      push_token(TokenType::Semicolon);
      break;
    case '*':
      push_token(TokenType::Star);
      break;
    // one or two character operators
    case '!':
      push_token(next_matches('=') ? TokenType::BangEqual : TokenType::Bang);
      break;
    case '=':
      push_token(next_matches('=') ? TokenType::Equal2 : TokenType::Equal);
      break;
    case '<':
      push_token(next_matches('=') ? TokenType::LessEqual : TokenType::Less);
      break;
    case '>':
      push_token(next_matches('=')
                   ? TokenType::GreaterEqual
                   : TokenType::Greater);
      break;
    case '/':
      if (next_matches('/')) {
        // '//' comments
        while (peek0() != '\n' && !is_at_end()) next();
      } else if (next_matches('*')) {
        push_block_comment();
      } else {
        push_token(TokenType::Slash);
      }
      break;
    // whitespace
    case ' ':
    case '\r':
    case '\t':
      break;
    case '\n':
      meta_new_line();
      break;
    case '"':
      push_string();
      break;
    default:
      if (std::isdigit(c)) {
        push_number();
      } else if (my_isalpha(c)) {
        push_identifier();
      } else {
        log_err(ErrorKind::Static, ErrorFatality::Fatal, meta,
                "Unexpected character.", {});
      }
      break;
  }
}

char Scanner::next() noexcept { return source[current++]; }

void Scanner::push_token(TokenType type, LiteralVal lit) noexcept {
  auto marked_text = mark_source();
  meta_update(marked_text);
  tokens.emplace_back(type, marked_text, lit, meta);
}

void Scanner::push_block_comment() noexcept {
  int counter = 1;
  while (!is_at_end() && counter != 0) {
    if (const auto peek = source.substr(current, 2); peek == "/*") {
      counter++;
    } else if (peek == "*/") {
      counter--;
    }
    if (peek0() == '\n') {
      meta_new_line();
    }
    next();
  }
  // Eat last /.
  next();
}

void Scanner::push_string() noexcept {
  while (peek0() != '"' && !is_at_end()) {
    // Strings can be multiline:
    if (peek0() == '\n') {
      meta_new_line();
    }
    next();
  }

  if (is_at_end()) {
    meta_update(mark_source());
    log_err(ErrorKind::Static, ErrorFatality::Fatal, meta,
            "Unterminated string.", {});
    return;
  }

  // Consume the closing quote.
  next();

  // Trim other surrounding quotes.
  const auto val = std::string{source.substr(start + 1, current - start - 2)};
  push_token(TokenType::String, LiteralVal{val});
}

void Scanner::push_number() noexcept {
  while (std::isdigit(peek0())) {
    next();
  }

  if (peek0() == '.' && std::isdigit(peek1f())) {
    next();
    while (std::isdigit(peek0())) {
      next();
    }
  }

  double val;
  const auto marked_text = mark_source();
  const auto start = marked_text.data();
  const auto end = start + marked_text.size();
  constexpr auto ok = std::errc{};
  if (const auto [ptr, errc] = std::from_chars(start, end, val); errc == ok) {
    push_token(TokenType::Number, LiteralVal{val});
  } else {
    meta_update(marked_text);
    log_err(ErrorKind::Static, ErrorFatality::Fatal, meta, "Invalid number.",
            {});
  }
}

void Scanner::push_identifier() noexcept {
  while (my_isalnum(peek0())) {
    next();
  }
  // unordered_maps don't work well with BetterEnum, but this method is also
  // okay if we don't have another scheme for identifying keywords.
  if (const auto marked_text = mark_source(); marked_text == "and"sv)
    push_token(TokenType::And);
  else if (marked_text == "class"sv)
    push_token(TokenType::Class);
  else if (marked_text == "else"sv)
    push_token(TokenType::Else);
  else if (marked_text == "false"sv)
    push_token(TokenType::False);
  else if (marked_text == "fun"sv)
    push_token(TokenType::Fun);
  else if (marked_text == "for"sv)
    push_token(TokenType::For);
  else if (marked_text == "if"sv)
    push_token(TokenType::If);
  else if (marked_text == "nil"sv)
    push_token(TokenType::Nil);
  else if (marked_text == "or"sv)
    push_token(TokenType::Or);
  else if (marked_text == "print"sv)
    push_token(TokenType::Print);
  else if (marked_text == "return"sv)
    push_token(TokenType::Return);
  else if (marked_text == "super"sv)
    push_token(TokenType::Super);
  else if (marked_text == "this"sv)
    push_token(TokenType::This);
  else if (marked_text == "true"sv)
    push_token(TokenType::True);
  else if (marked_text == "var"sv)
    push_token(TokenType::Var);
  else if (marked_text == "while"sv)
    push_token(TokenType::While);
  else
    push_token(TokenType::Identifier);
}

bool Scanner::next_matches(char expected) noexcept {
  if (is_at_end()) return false;
  if (source[current] != expected) return false;
  current++;
  return true;
}

char Scanner::peek0() const noexcept {
  if (is_at_end()) return '\0';
  return source[current];
}

char Scanner::peek1f() const noexcept {
  if (current + 1 >= source.size()) return '\0';
  return source[current + 1];
}

bool Scanner::is_at_end() const { return current >= source.size(); }

void Scanner::meta_new_line() noexcept {
  meta.line++;
  line_start = current;
}

void Scanner::meta_update(const std::string_view text) noexcept {
  meta.column = current - line_start;
  meta.length = text.size();
}

std::string_view Scanner::mark_source() const noexcept {
  return source.substr(start, current - start);
}

static bool my_isalpha(char c) { return std::isalpha(c) || c == '_'; }

static bool my_isalnum(char c) { return std::isdigit(c) || my_isalpha(c); }
} // namespace lox