//
// Created by vivraan on 12-01-2025.
//

#include <print>
#include "interpreter.h"

#include "log_err.h"

namespace lox::interpreter {
void Interpreter::interpret(const Expr* expr_tree) {
  try {
    auto result = eval(expr_tree);
    std::println("{}", result);
  } catch (const RuntimeError&) {
    // Do nothing, just log the error (see make_error).
    // This is kind of like "expecting" an error, and can be represented by values (a la std::expected<>).
    // Alas, it's pretty boring to do and debug in C++ without syntax sugar.
  }
}

LiteralVal Interpreter::eval(const Expr* expr_tree) {
  return std::visit(
      [this]<typename ExprU>(ExprU&& expr) -> LiteralVal {
        if constexpr (std::is_same_v<ExprU, GroupingExpr>) {
          return visit_group(expr);
        } else if constexpr (std::is_same_v<ExprU, UnaryOpExpr>) {
          return visit_unop(expr);
        } else if constexpr (std::is_same_v<ExprU, BinaryOpExpr>) {
          return visit_binop(expr);
        } else if constexpr (std::is_same_v<ExprU, TernaryOpExpr>) {
          return visit_tern(expr);
        } else if constexpr (std::is_same_v<ExprU, LiteralExpr>) {
          return visit_lit(expr);
        }
        // Panic really hard if you ever reach here (applies to all other uses).
        std::unreachable();
      },
      *expr_tree);
}

LiteralVal Interpreter::visit_group(const GroupingExpr& group) {
  return eval(group.inner.get());
}

LiteralVal Interpreter::visit_unop(const UnaryOpExpr& unop) {
  const auto r = eval(unop.r.get());

  switch (unop.op.t) {
  case TokenType::Bang:
    return LiteralVal{!is_truthy(r)};
  case TokenType::Minus: {
    if (const auto r_d = std::get_if<double>(&r)) {
      return LiteralVal{-1 * *r_d};
    }
    throw make_error(unop.op, "Operand must be a number.");
  }
  default:
    std::unreachable();
  }
}

LiteralVal Interpreter::visit_binop(const BinaryOpExpr& binop) {
  auto l = eval(binop.l.get());
  auto r = eval(binop.r.get());

  // handle strings separately
  const auto l_str = std::get_if<std::string>(&l);
  // ReSharper disable once CppTooWideScopeInitStatement
  const auto r_str = std::get_if<std::string>(&r);
  if (const auto
        l_str = std::get_if<std::string>(&l),
        r_str = std::get_if<std::string>(&r);
    l_str && r_str) {
    switch (binop.op.t) {
    case TokenType::Plus:
      return LiteralVal{std::format("{}{}", *l_str, *r_str)};
    // strings are truthy, TODO but not lexically comparable just yet
    case TokenType::Equal2:
    case TokenType::BangEqual:
      break;
    default:
      throw make_error(binop.op, "Operands must be strings.");
    }
  }

  // if not strings, numbers
  if (const auto l_d = std::get_if<double>(&l), r_d = std::get_if<double>(&r);
    l_d && r_d) {
    switch (binop.op.t) {
    case TokenType::Plus:
      return LiteralVal{*l_d + *r_d};
    case TokenType::Minus:
      return LiteralVal{*l_d - *r_d};
    case TokenType::Slash: {
      if (*r_d == 0) {
        throw make_error(binop.op, "Division by zero.");
      }
      return LiteralVal{*l_d / *r_d};
    }
    case TokenType::Star:
      return LiteralVal{*l_d * *r_d};
    case TokenType::Greater:
      return LiteralVal{*l_d > *r_d};
    case TokenType::GreaterEqual:
      return LiteralVal{*l_d >= *r_d};
    case TokenType::Less:
      return LiteralVal{*l_d < *r_d};
    case TokenType::LessEqual:
      return LiteralVal{*l_d <= *r_d};
    case TokenType::Equal2:
    case TokenType::BangEqual:
      break;
    default:
      throw make_error(binop.op, "Operands must be numbers.");
    }
  }

  // After all else, if truthy, we can compare
  switch (binop.op.t) {
  // Here, we rely on the std behaviour for std::variant comparator overloads
  case TokenType::Equal2:
    return LiteralVal{l == r};
  case TokenType::BangEqual:
    return LiteralVal{l != r};
  default:
    std::unreachable();
  }
}

LiteralVal Interpreter::visit_tern(const TernaryOpExpr& tern) {
  if (const auto cond = eval(tern.cond.get()); is_truthy(cond)) {
    return eval(tern.t.get());
  }
  return eval(tern.f.get());
}

LiteralVal Interpreter::visit_lit(const LiteralExpr& lit) {
  return lit.k.v;
}

bool Interpreter::is_truthy(const LiteralVal& lit) noexcept {
  // Go with Bob Nystrom and Ruby on this one
  if (std::get_if<std::monostate>(&lit)) {
    return false;
  }
  if (auto* b = std::get_if<bool>(&lit)) {
    return *b;
  }
  return true;
}

RuntimeError Interpreter::make_error(const Token& k, const std::string& msg) {
  log_err(ErrorKind::Runtime, ErrorFatality::Fatal, k, msg);
  return RuntimeError{};
}
} // namespace lox::interpreter