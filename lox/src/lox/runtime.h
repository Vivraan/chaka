//
// Created by vivraan on 01-01-2025.
//

#ifndef RUNTIME_H
#define RUNTIME_H
#include <print>
#include <string_view>

#include "interpreter.h"

namespace lox::runtime {
interpreter::Interpreter interpreter;

void run_file(std::string_view path);
void run_prompt();
static void run(std::string_view source);
} // namespace lox

#endif  // RUNTIME_H