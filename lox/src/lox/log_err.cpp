//
// Created by shiva on 01-01-2025.
//

#include "log_err.h"

#include <print>

#include "token.h"

namespace lox {
std::bitset<ErrorKind::_size() + 1> had_error;

void log_err(ErrorKind kind, ErrorFatality fatality, LogMetadata meta,
             std::string_view ctx = "", std::string_view msg = "") noexcept {
  auto fmt_kind = kind._to_index() == ErrorKind::Static ? "static" : "runtime";
  auto fmt_fatality = fatality._to_index() == ErrorFatality::Fatal
                        ? "fatal"
                        : "non-fatal";
  auto fmt_msg = msg.empty() ? "<unspecified>" : msg;
  if (meta.length > 1) {
    auto start = meta.column;
    auto end = meta.column + meta.length - 1;
    std::println(stderr, "[{}:{}-{}] {} {} error{}: {}", meta.line, start, end,
                 fmt_fatality, fmt_kind, ctx, fmt_msg);
  } else {
    std::println(stderr, "[{}:{}] {} {} error{}: {}", meta.line, meta.column,
                 fmt_fatality,
                 fmt_kind, ctx, fmt_msg);
  }
  had_error.set(kind);
  had_error.set(3, fatality);
}

void log_err(ErrorKind kind, ErrorFatality fatality, const Token& token,
             std::string_view msg) noexcept {
  using namespace std::string_view_literals;
  using namespace token;
  const std::string_view ctx = token.t._to_index() == TokenType::Eof
                                 ? " at end"sv
                                 : std::format(" at '{}'", token.lex);
  log_err(kind, fatality, token.meta, ctx, msg);
}
} // namespace lox