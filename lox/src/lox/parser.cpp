//
// Created by vivraan on 07-01-2025.
//

#include "parser.h"

#include <algorithm>

#include "log_err.h"

namespace lox::parser {
// TODO await stmt implementation
const Expr* Parser::parse_syntax() {
  try {
    if (!expr_tree) {
      expr_tree = parse_expr();
    }
    return expr_tree.get();
  } catch (const ParserError&) {
    return nullptr;
  }
}

ExprOwnPtr Parser::parse_expr() {
  // These binary operators can't be the first token in an expression.
  if (next_matches({TokenType::Slash, TokenType::Star, TokenType::Plus})) {
    throw make_error(peek1b(), "Illegal binary operator, expected expression.");
  }
  return parse_comma();
}

ExprOwnPtr Parser::parse_comma() {
  // comma -> (expression ",")* expression
  ExprOwnPtr result = parse_tern();
  while (next_matches({TokenType::Comma})) {
    result = parse_tern();
  }
  return result;
}

ExprOwnPtr Parser::parse_tern() {
  // ternary -> equality ("?" expression ":" expression)?
  ExprOwnPtr eq_expr = parse_eql();
  // A ternary expression could just remain an equality expression.
  if (!next_matches({TokenType::Question})) return eq_expr;
  ExprOwnPtr t_expr = parse_expr();
  consume(TokenType::Colon, "Expected ':' after '?' in ternary expression.");
  ExprOwnPtr f_expr = parse_expr();
  return make_tern(std::move(eq_expr), std::move(t_expr), std::move(f_expr));
}

ExprOwnPtr Parser::parse_eql() {
  // equality -> comparison (("!=" | "==") comparison)*
  ExprOwnPtr expr = parse_cmp();

  while (next_matches({TokenType::BangEqual, TokenType::Equal2})) {
    Token op = peek1b();
    ExprOwnPtr r = parse_cmp();
    expr = make_binop(op, std::move(expr), std::move(r));
  }

  return expr;
}

ExprOwnPtr Parser::parse_cmp() {
  // comparison -> addition (">" | ">=" | "<" | "<=" addition)*
  return parse_binop_l2r({TokenType::Less, TokenType::LessEqual,
                          TokenType::Greater, TokenType::GreaterEqual},
                         &Parser::parse_add);
}

ExprOwnPtr Parser::parse_add() {
  // addition -> multiplication (("-" | "+") multiplication)*
  return parse_binop_l2r({TokenType::Minus, TokenType::Plus},
                         &Parser::parse_mul);
}

ExprOwnPtr Parser::parse_mul() {
  // multiplication -> unary (("/" | "*") unary)*
  return parse_binop_l2r({TokenType::Slash, TokenType::Star},
                         &Parser::parse_unop);
}

ExprOwnPtr Parser::parse_unop() {
  // unary -> ("!" | "-" | "+") unary | primary
  if (next_matches({TokenType::Bang, TokenType::Minus})) {
    const Token op = peek1b();
    ExprOwnPtr r = parse_unop();
    return make_unop(op, std::move(r));
  }
  return parse_primary();
}

ExprOwnPtr Parser::parse_primary() {
  // primary -> "false" | "true" | "nil" | NUMBER | STRING | "(" expression ")"
  // Parsing what the scanner didn't.
  if (next_matches({TokenType::Nil})) {
    auto lit_tok = peek1b();
    lit_tok.v = LiteralNil;
    return make_lit(lit_tok);
  }
  if (next_matches({TokenType::True})) {
    auto lit_tok = peek1b();
    lit_tok.v = LiteralVal{true};
    return make_lit(lit_tok);
  }
  if (next_matches({TokenType::False})) {
    auto lit_tok = peek1b();
    lit_tok.v = LiteralVal{false};
    return make_lit(lit_tok);
  }

  if (next_matches({TokenType::Number, TokenType::String})) {
    return make_lit(peek1b());
  }

  if (next_matches({TokenType::LParen})) {
    ExprOwnPtr expr = parse_expr();
    consume(TokenType::RParen, "Expected ')' after expression.");
    return make_group(std::move(expr));
  }

  // We shouldn't be here haha ~^~.
  throw make_error(peek0(), "Expected expression.");
}

ExprOwnPtr Parser::parse_binop_l2r(std::initializer_list<TokenType> token_ts,
                                   ParseFn parse_higher_rule) {
  // [[ binary -> next_higher_rule (matches_list next_higher_rule)* ]]
  ExprOwnPtr l_base = (this->*parse_higher_rule)();
  while (next_matches(token_ts)) {
    // For right-recursion, the left operand and operator must always be valid.
    Token op = peek1b();
    ExprOwnPtr r = (this->*parse_higher_rule)();
    l_base = make_binop(op, std::move(l_base), std::move(r));
  }
  return l_base;
}

bool Parser::next_matches(std::initializer_list<TokenType> token_ts) {
  if (std::ranges::any_of(token_ts, [this](TokenType t) { return check(t); })) {
    next();
    return true;
  }
  return false;
}

bool Parser::check(TokenType t) const {
  if (is_at_end()) return false;
  return peek0().t == t;
}

Token Parser::next() {
  if (!is_at_end()) ++current;
  return peek1b();
}

bool Parser::is_at_end() const {
  return peek0().t._to_index() == TokenType::Eof;
}

Token Parser::peek0() const { return scanned_tokens[current]; }

Token Parser::peek1b() const { return scanned_tokens[current - 1]; }

Token Parser::consume(TokenType t, const std::string_view msg) {
  if (check(t)) return next();
  throw make_error(peek0(), msg);
}

void Parser::sync_from_err() {
  next();

  while (!is_at_end()) {
    // TODO await stmt implementation -- see below
    // if (peek1b().t._to_index() == TokenType::Semicolon) return;

    switch (peek1b().t) {
      // TODO see above
      case TokenType::Semicolon:
      //
      case TokenType::Class:
      case TokenType::Fun:
      case TokenType::Var:
      case TokenType::For:
      case TokenType::If:
      case TokenType::While:
      case TokenType::Print:
      case TokenType::Return:
        return;
      default:
        next();
        break;
    }
  }
}

ParserError Parser::make_error(const Token& token, std::string_view msg) {
  log_err(ErrorKind::Static, ErrorFatality::Fatal, token, msg);
  return ParserError{};
}
} // namespace lox::parser