//
// Created by vivraan on 05-01-2025.
//

#include "expr.h"

namespace lox::expr {
ExprOwnPtr make_group(ExprOwnPtr inner) {
  return std::make_unique<Expr>(GroupingExpr{std::move(inner)});
}

ExprOwnPtr make_binop(const Token& op, ExprOwnPtr l, ExprOwnPtr r) {
  return std::make_unique<Expr>(BinaryOpExpr{op, std::move(l), std::move(r)});
}

ExprOwnPtr make_unop(const Token& op, ExprOwnPtr r) {
  return std::make_unique<Expr>(UnaryOpExpr{op, std::move(r)});
}

ExprOwnPtr make_tern(ExprOwnPtr cond, ExprOwnPtr t, ExprOwnPtr f) {
  return std::make_unique<Expr>(
      TernaryOpExpr{std::move(cond), std::move(t), std::move(f)});
}

ExprOwnPtr make_lit(const Token& k) {
  return std::make_unique<Expr>(LiteralExpr{k});
}

constexpr std::string_view group_str = "<group>";

std::string parens(std::string_view name,
                   std::initializer_list<const Expr*> exprs) noexcept {
  std::string str = "(";
  str += name;
  for (const auto& expr : exprs) {
    str += " ";
    str += to_string(expr);
  }
  str += ")";
  return str;
}

std::string to_string(const Expr* expr_tree) noexcept {
  if (expr_tree == nullptr) {
    return "<Empty>";
  }
  return std::visit(
      [&]<typename ExprU>(ExprU&& expr) -> std::string {
        using ExprT = std::decay_t<ExprU>;
        if constexpr (std::is_same_v<ExprT, GroupingExpr>) {
          return parens(group_str, {expr.inner.get()});
        } else if constexpr (std::is_same_v<ExprT, UnaryOpExpr>) {
          return parens(expr.op.lex, {expr.r.get()});
        } else if constexpr (std::is_same_v<ExprT, BinaryOpExpr>) {
          return parens(expr.op.lex, {expr.l.get(), expr.r.get()});
        } else if constexpr (std::is_same_v<ExprT, TernaryOpExpr>) {
          return parens("?:", {expr.cond.get(), expr.t.get(), expr.f.get()});
        } else if constexpr (std::is_same_v<ExprT, LiteralExpr>) {
          return std::format("{}", expr.k);
        } else {
          return std::string{"<Undefined expression type>"};
        }
      },
      *expr_tree);
}
} // namespace lox::expr