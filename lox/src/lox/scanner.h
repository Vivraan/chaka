//
// Created by vivraan on 01-01-2025.
//

#ifndef SCANNER_H
#define SCANNER_H
#include <span>
#include <vector>

#include <gsl/gsl>

#include "token.h"

namespace lox::scanner {
using namespace std::string_view_literals;
using namespace token;
using token::Token;

class Scanner final {
 public:
  explicit Scanner(const std::string_view source) : source(source) {}

  std::span<Token> scan_tokens() noexcept;

 private:
  [[nodiscard]] bool is_at_end() const;
  void scan_token() noexcept;
  char next() noexcept;
  void push_token(TokenType type, LiteralVal lit = LiteralNil) noexcept;
  void push_block_comment() noexcept;
  void push_string() noexcept;
  void push_number() noexcept;
  void push_identifier() noexcept;
  bool next_matches(char expected) noexcept;
  [[nodiscard]] char peek0() const noexcept;
  [[nodiscard]] char peek1f() const noexcept;
  void meta_new_line() noexcept;
  void meta_update(std::string_view text) noexcept;
  [[nodiscard]] std::string_view mark_source() const noexcept;

  std::string_view source;
  std::vector<Token> tokens;
  gsl::index start = 0;
  gsl::index current = 0;
  gsl::index line_start = 0;
  LogMetadata meta;
};
}  // namespace lox::scanner

#endif  // SCANNER_H