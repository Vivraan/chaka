//
// Created by vivraan on 12-01-2025.
//

#ifndef INTERPRETER_H
#define INTERPRETER_H
#include "expr.h"
#include "token.h"

namespace lox::interpreter {
using namespace lox::expr;
using namespace lox::token;

class RuntimeError final : std::exception {
};

class Interpreter final {
public:
  void interpret(const Expr* expr_tree);

private:
  LiteralVal eval(const Expr* expr_tree);
  LiteralVal visit_group(const GroupingExpr& group);
  LiteralVal visit_unop(const UnaryOpExpr& unop);
  LiteralVal visit_binop(const BinaryOpExpr& binop);
  LiteralVal visit_tern(const TernaryOpExpr& tern);
  static LiteralVal visit_lit(const LiteralExpr& lit);

  // This is Lox, calm down sailor, don't get mad, they use Ruby's rule.
  static bool is_truthy(const LiteralVal& lit) noexcept;

  static RuntimeError make_error(const Token& k, const std::string& msg);
};
} // namespace lox::interpreter

#endif  // INTERPRETER_H