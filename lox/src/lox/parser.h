//
// Created by vivraan on 07-01-2025.
//

#ifndef PARSER_H
#define PARSER_H
#include <gsl/util>
#include <span>

#include "expr.h"
#include "token.h"

namespace lox::parser {
using namespace lox::token;
using namespace lox::expr;
using token::Token;

class ParserError final : std::exception {
};

class Parser final {
public:
  explicit Parser(const std::span<Token> tokens) : scanned_tokens(tokens) {
  }

  const Expr* parse_syntax();

private:
  ExprOwnPtr parse_expr();
  ExprOwnPtr parse_comma();
  ExprOwnPtr parse_tern();
  ExprOwnPtr parse_eql();
  ExprOwnPtr parse_cmp();
  ExprOwnPtr parse_add();
  ExprOwnPtr parse_mul();
  ExprOwnPtr parse_unop();
  ExprOwnPtr parse_primary();

  using ParseFn = ExprOwnPtr (Parser::*)();
  ExprOwnPtr parse_binop_l2r(std::initializer_list<TokenType> token_ts,
                             ParseFn parse_higher_rule);

  bool next_matches(std::initializer_list<TokenType> token_ts);
  [[nodiscard]] bool check(TokenType t) const;
  Token next();
  [[nodiscard]] bool is_at_end() const;
  [[nodiscard]] Token peek0() const;
  [[nodiscard]] Token peek1b() const;
  Token consume(TokenType t, std::string_view msg);
  void sync_from_err();

  static ParserError make_error(const Token& token, std::string_view msg);

  std::span<Token> scanned_tokens;
  ExprOwnPtr expr_tree{};
  gsl::index current = 0;
};
} // namespace lox::parser

#endif  // PARSER_H