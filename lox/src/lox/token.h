//
// Created by shiva on 01-01-2025.
//

#ifndef TOKEN_H
#define TOKEN_H
#include "thirdparty/better_enums_0_11_3.h"

#include <format>
#include <iostream>
#include <span>
#include <string>
#include <variant>

namespace lox::token {
struct LogMetadata final {
  size_t line = 0;
  size_t column = 0;
  size_t length = 0;
};

SLOW_ENUM(TokenType, int,
          // One character tokens
          LParen, // (
          RParen, // )
          LBrace, // {
          RBrace, // }
          Colon, // :
          Comma, // ,
          Dot, // .
          Minus, // -
          Plus, // +
          Question, // ?
          Semicolon, // ;
          Slash, // /
          Star, // *
          // One or two character tokens
          Bang, // !
          BangEqual, // !=
          Equal, // =
          Equal2, // ==
          Greater, // >
          GreaterEqual, // >=
          Less, // <
          LessEqual, // <=
          // Literals
          Identifier, String, Number,
          // Keywords
          And, Class, Else, False, Fun, For, If, Nil, Or, Print, Return, Super,
          This, True, Var, While,
          // End of file
          Eof);

struct LiteralVal final
    : std::variant<std::monostate, std::string, double, bool> {
};

constexpr LiteralVal LiteralNil{std::monostate{}};

struct Token final {
  TokenType t;
  std::string_view lex;
  LiteralVal v;
  LogMetadata meta;
};
} // namespace lox::token

// Specialise some formatters for output

template <>
struct std::formatter<lox::token::LiteralVal>
    : std::formatter<std::string_view> {
  auto format(const lox::token::LiteralVal lit_val, auto& ctx) const {
    return std::visit(
        [&ctx]<typename LitU>(LitU&& v) {
          using LitT = std::decay_t<LitU>;
          if constexpr (std::is_same_v<LitT, std::monostate>) {
            return std::format_to(ctx.out(), "nil");
          } else if constexpr (std::is_same_v<LitT, std::string>) {
            return std::format_to(ctx.out(), R"("{}")", v);
          } else if constexpr (std::is_same_v<LitT, double>) {
            return std::format_to(ctx.out(), "{}", v);
          } else if constexpr (std::is_same_v<LitT, bool>) {
            return std::format_to(ctx.out(), "{}", v ? "true" : "false");
          }
          std::unreachable();
        },
        lit_val);
  }
};

template <>
struct std::formatter<lox::token::Token> : std::formatter<std::string_view> {
  auto format(const lox::token::Token& token, auto& ctx) const {
    return std::format_to(ctx.out(), "{} ({})", token.lex,
                          token.t._to_string());
  }
};

#endif // TOKEN_H