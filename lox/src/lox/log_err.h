//
// Created by shiva on 01-01-2025.
//

#ifndef LOX_ERROR_H
#define LOX_ERROR_H
#include <bitset>

#include "thirdparty/better_enums_0_11_3.h"
#include "token.h"

namespace lox {
using namespace token;

SLOW_ENUM(ErrorKind, int, Other = 0b00, Static = 0b01, Runtime = 0b10,
          /// Only use in conditionals, not logging
          Any = Static | Runtime);
SLOW_ENUM(ErrorFatality, int, NonFatal = 0b0, Fatal = 0b1);

extern std::bitset<ErrorKind::_size() + 1> had_error;
void log_err(ErrorKind, ErrorFatality, LogMetadata, std::string_view ctx,
             std::string_view msg) noexcept;

void log_err(ErrorKind, ErrorFatality, const Token&,
             std::string_view msg) noexcept;
} // namespace lox

#endif  // LOX_ERROR_H