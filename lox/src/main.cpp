//
// Created by vivraan on 04-06-2023.
//

#include <print>

#include "lox/runtime.h"

int main(int argc, char** argv) {
  switch (argc) {
    case 1:
      lox::run_prompt();
      return 0;
    case 2:
      lox::run_file(argv[1]);
      return 0;
    default:
      std::println("Usage: cpplox [script]");
      return 64;
  }
}
