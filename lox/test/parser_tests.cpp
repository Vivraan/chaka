//
// Created by vivraan on 11-01-2025.
//

#include <gtest/gtest.h>

#include "lox/parser.h"
#include "lox/scanner.h"

using namespace lox::expr;
using namespace lox::parser;
using namespace lox::scanner;

TEST(ParserTestsSuite, Basic) {
  constexpr std::string_view source = R"(3 + 4 * (2 - 1) / 5 - 6)";

  Scanner scanner{source};
  const auto tokens = scanner.scan_tokens();
  Parser parser{tokens};
  const auto expr_tree = parser.parse_syntax();
  EXPECT_EQ(
      to_string(expr_tree),
      R"((- (+ 3 (Number) (/ (* 4 (Number) (<group> (- 2 (Number) 1 (Number)))) 5 (Number))) 6 (Number)))");
}

TEST(ParserTestsSuite, IllegalBinaryOps) {
  constexpr auto sources = std::to_array<std::string_view>({
      R"(+ 3 / 2)",
      R"(* 3 + 2)",
      R"(/ 3 - 2)",
  });

  for (const auto& source : sources) {
    Scanner scanner{source};
    Parser parser{scanner.scan_tokens()};

    testing::internal::CaptureStderr();
    EXPECT_EQ(parser.parse_syntax(), nullptr);
    const std::string err_log = testing::internal::GetCapturedStderr();
    EXPECT_TRUE(err_log.contains("Illegal binary operator"));
  }
}

TEST(ParserTestsSuite, Ternary) {
  constexpr std::string_view source = "3 == 3 ? 4 : 5";

  Scanner scanner{source};
  const auto tokens = scanner.scan_tokens();
  Parser parser{tokens};
  const auto expr_tree = parser.parse_syntax();
  EXPECT_EQ(to_string(expr_tree),
            R"((?: (== 3 (Number) 3 (Number)) 4 (Number) 5 (Number)))");
}

TEST(ParserTestsSuite, TernaryNested) {
  constexpr std::string_view source = "3 == 3 ? 4 == 4 ? 5 : 6 : 7";

  Scanner scanner{source};
  const auto tokens = scanner.scan_tokens();
  Parser parser{tokens};
  const auto expr_tree = parser.parse_syntax();
  EXPECT_EQ(
      to_string(expr_tree),
      R"((?: (== 3 (Number) 3 (Number)) (?: (== 4 (Number) 4 (Number)) 5 (Number) 6 (Number)) 7 (Number)))");
}

TEST(ParserTestSuite, Comma) {
  constexpr std::string_view source = "3, 4, 5";

  Scanner scanner{source};
  const auto tokens = scanner.scan_tokens();
  Parser parser{tokens};
  const auto expr_tree = parser.parse_syntax();
  EXPECT_EQ(to_string(expr_tree), R"(5 (Number))");
}

TEST(ParserTestSuite, CommaComplex) {
  constexpr std::string_view source = "3 + 4, 5 * (6 - 7), 8 / 9";

  Scanner scanner{source};
  const auto tokens = scanner.scan_tokens();
  Parser parser{tokens};
  const auto expr_tree = parser.parse_syntax();
  EXPECT_EQ(to_string(expr_tree), R"((/ 8 (Number) 9 (Number)))");
}