//
// Created by vivraan on 14-01-2025.
//

#include <gtest/gtest.h>

#include "lox/interpreter.h"
#include "lox/parser.h"
#include "lox/scanner.h"

using namespace lox::interpreter;
using namespace lox::token;
using namespace lox::expr;
using namespace lox::parser;
using namespace lox::scanner;

TEST(IntepreterTestsSuite, TestCorrectExprSyntax) {
  constexpr std::string_view source = R"(3 + 4 * (2 - 1) / 5 - 6)";

  Scanner scanner{source};
  Parser parser{scanner.scan_tokens()};

  ::testing::internal::CaptureStdout();

  Interpreter interpreter;
  interpreter.interpret(parser.parse_syntax());

  const auto output = ::testing::internal::GetCapturedStdout();
  EXPECT_EQ(output, "-2.2\n");
}