//
// Created by vivraan on 17-06-2023.
//

#include <gtest/gtest.h>

#include <array>
#include <numeric>
#include <print>
#include <span>

#include "lox/scanner.h"
#include "lox/token.h"

using namespace lox::token;
using namespace lox::scanner;

namespace lox::testing {

struct TokenSpanPrintHelper {
  const std::span<Token>& tokens;
};

std::ostream& operator<<(std::ostream& os, const TokenSpanPrintHelper& helper) {
  os << "[\n";
  for (const auto& token : helper.tokens) {
    std::print(os, "{}", token);
  }
  os << "]";
  return os;
}
}

TEST(ScannerTestsSuite, RandomChars) {
  using namespace lox::testing;
  constexpr std::string_view source = R"(
//
{}(){{}
The "quick" brown_fox jump3d over th3 lazy dogg... 3ce (3.0 times)
)";

  // Requires the type for TokenType, otherwise it assumes it to be
  // TokenType::_enumerated. This version deduces the number of arguments.
  constexpr auto test_token_types = std::to_array<TokenType>(
  {
      TokenType::LParen, TokenType::RBrace, TokenType::LParen,
      TokenType::RParen, TokenType::LBrace, TokenType::LBrace,
      TokenType::RBrace,
      //
      TokenType::Identifier, TokenType::String, TokenType::Identifier,
      TokenType::Identifier, TokenType::Identifier, TokenType::Identifier,
      TokenType::Identifier, TokenType::Identifier, TokenType::Dot,
      TokenType::Dot, TokenType::Dot, TokenType::Number, TokenType::Identifier,
      TokenType::LParen, TokenType::Number, TokenType::Identifier,
      TokenType::RParen,
      ////
      TokenType::Eof
  });

  Scanner scanner{source};
  const auto tokens = scanner.scan_tokens();

  for (auto i = 0; i < tokens.size(); i++) {
    if (test_token_types[i] != tokens[i].t) {
      std::print("Expected: {}\n", test_token_types[i]._to_string());
      std::print("Actual: {}\n", tokens[i].t._to_string());
      FAIL() << TokenSpanPrintHelper{tokens};
    }
  }
}

TEST(ScannerTestsSuite, BlockComments) {
  using namespace lox::testing;
  constexpr std::string_view source = R"(
    /* /* yaw */ */
  )";

  Scanner scanner{source};
  const auto tokens = scanner.scan_tokens();
  EXPECT_EQ(TokenType::Eof, tokens[0].t._to_integral())
    << TokenSpanPrintHelper{tokens};
}