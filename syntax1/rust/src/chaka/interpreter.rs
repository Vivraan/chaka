/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                +
+                                                                                                +
+ Permission is hereby granted, free of charge, to any person obtaining a                        +
+ copy of this software and associated documentation files (the                                  +
+ “Software”), to deal in the Software without restriction, including without                    +
+ limitation the rights to use, copy, modify, merge, publish, distribute,                        +
+ sublicense, and/or sell copies of the Software, and to permit persons to                       +
+ whom the Software is furnished to do so, subject to the following                              +
+ conditions:                                                                                    +
+                                                                                                +
+ The above copyright notice and this permission notice shall be included                        +
+ in all copies or substantial portions of the Software.                                         +
+                                                                                                +
+ THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                      +
+ KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                     +
+ WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                        +
+ PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                     +
+ OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                       +
+ OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                     +
+ OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                      +
+ SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                         +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

use std::num::FpCategory;

use once_cell::sync::Lazy;
use posix_errors::{EOPNOTSUPP, posix_error, PosixError};

use crate::chaka::error::{error, ErrorKind};
use crate::chaka::expr::{Expr, ExprTree};
use crate::chaka::token::{Literal, Token, TokenKind};

static OPNOTSUPP: Lazy<PosixError, fn() -> PosixError> = Lazy::new(|| posix_error(EOPNOTSUPP, "operation not supported"));

fn eval(expr_tree: ExprTree<'_>) -> Result<Literal<'_>, PosixError> {
  match expr_tree.0 {
    // This is a syntax error, not a runtime error.
    None => Ok(Literal::MonoState),
    Some(expr) => match expr.as_ref() {
      Expr::Unary(op, right) => {
        let invalid_operand = |op: &Token<'_>| {
          let msg = "Invalid operand for unary operator.";
          error(ErrorKind::Runtime, op.log_info, msg);
          Err(OPNOTSUPP.clone())
        };
        match eval(ExprTree::x(&right)) {
          Ok(val) => match val {
            Literal::Bool(b) => match op.kind {
              TokenKind::Bang => Ok(Literal::Bool(!b)),
              _ => invalid_operand(&op),
            },
            Literal::Int(i) => match op.kind {
              TokenKind::Tilde => Ok(Literal::Int(!i)),
              TokenKind::Minus => Ok(Literal::Int(-i)),
              TokenKind::Plus => Ok(Literal::Int(i)),
              _ => invalid_operand(&op),
            },
            Literal::Float(f) => match op.kind {
              TokenKind::Minus => Ok(Literal::Float(-f)),
              TokenKind::Plus => Ok(Literal::Float(f)),
              _ => invalid_operand(&op),
            },
            _ => invalid_operand(&op),
          },
          Err(e) => Err(e),
        }
      }
      Expr::Binary(left, op, right) => {
        let invalid_operands = |op: &Token<'_>| {
          let msg = "Invalid operands for binary operator.";
          error(ErrorKind::Runtime, op.log_info, msg);
          Err(OPNOTSUPP.clone())
        };
        match (eval(ExprTree::x(&left)), eval(ExprTree::x(&right))) {
          (Err(e), _) => Err(e),
          (_, Err(e)) => Err(e),
          (Ok(left_val), Ok(right_val)) => match op.clone().kind {
            TokenKind::Plus => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Int(l + r)),
              (Literal::Int(l), Literal::Float(r)) => Ok(Literal::Float((l as f64) + r)),
              (Literal::Float(l), Literal::Int(r)) => Ok(Literal::Float(l + (r as f64))),
              (Literal::Float(l), Literal::Float(r)) => Ok(Literal::Float(l + r)),
              _ => invalid_operands(&op),
            }
            TokenKind::Minus => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Int(l - r)),
              (Literal::Int(l), Literal::Float(r)) => Ok(Literal::Float((l as f64) - r)),
              (Literal::Float(l), Literal::Int(r)) => Ok(Literal::Float(l - (r as f64))),
              (Literal::Float(l), Literal::Float(r)) => Ok(Literal::Float(l - r)),
              _ => invalid_operands(&op),
            }
            TokenKind::Star => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Int(l * r)),
              (Literal::Int(l), Literal::Float(r)) => Ok(Literal::Float((l as f64) * r)),
              (Literal::Float(l), Literal::Int(r)) => Ok(Literal::Float(l * (r as f64))),
              (Literal::Float(l), Literal::Float(r)) => Ok(Literal::Float(l * r)),
              _ => invalid_operands(&op),
            }
            TokenKind::Slash => {
              let div_by_zero = |op: &Token<'_>| {
                let msg = "Division by zero.";
                error(ErrorKind::Runtime, op.log_info, msg);
                Err(OPNOTSUPP.clone())
              };
              match right_val {
                Literal::Int(0) => div_by_zero(&op),
                Literal::Float(r) if r.classify() == FpCategory::Zero => div_by_zero(&op),
                _ => Ok(()),
              }?;
              match (left_val, right_val) {
                (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Int(l / r)),
                (Literal::Int(l), Literal::Float(r)) => Ok(Literal::Float((l as f64) / r)),
                (Literal::Float(l), Literal::Int(r)) => Ok(Literal::Float(l / (r as f64))),
                (Literal::Float(l), Literal::Float(r)) => Ok(Literal::Float(l / r)),
                _ => invalid_operands(&op),
              }
            }
            TokenKind::Caret => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Int(l ^ r)),
              _ => invalid_operands(&op),
            }
            TokenKind::Ampersand => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Int(l & r)),
              _ => invalid_operands(&op),
            }
            TokenKind::Pipe => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Int(l | r)),
              _ => invalid_operands(&op),
            }
            TokenKind::RAngle => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Bool(l > r)),
              (Literal::Int(l), Literal::Float(r)) => Ok(Literal::Bool((l as f64) > r)),
              (Literal::Float(l), Literal::Int(r)) => Ok(Literal::Bool(l > (r as f64))),
              (Literal::Float(l), Literal::Float(r)) => Ok(Literal::Bool(l > r)),
              _ => invalid_operands(&op),
            },
            TokenKind::RAngleEql => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Bool(l >= r)),
              (Literal::Int(l), Literal::Float(r)) => Ok(Literal::Bool((l as f64) >= r)),
              (Literal::Float(l), Literal::Int(r)) => Ok(Literal::Bool(l >= (r as f64))),
              (Literal::Float(l), Literal::Float(r)) => Ok(Literal::Bool(l >= r)),
              _ => invalid_operands(&op),
            },
            TokenKind::LAngle => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Bool(l < r)),
              (Literal::Int(l), Literal::Float(r)) => Ok(Literal::Bool((l as f64) < r)),
              (Literal::Float(l), Literal::Int(r)) => Ok(Literal::Bool(l < (r as f64))),
              (Literal::Float(l), Literal::Float(r)) => Ok(Literal::Bool(l < r)),
              _ => invalid_operands(&op),
            },
            TokenKind::LAngleEql => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Bool(l <= r)),
              (Literal::Int(l), Literal::Float(r)) => Ok(Literal::Bool((l as f64) <= r)),
              (Literal::Float(l), Literal::Int(r)) => Ok(Literal::Bool(l <= (r as f64))),
              (Literal::Float(l), Literal::Float(r)) => Ok(Literal::Bool(l <= r)),
              _ => invalid_operands(&op),
            },
            TokenKind::Eql2 => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Bool(l == r)),
              (Literal::Int(l), Literal::Float(r)) => Ok(Literal::Bool((l as f64) == r)),
              (Literal::Float(l), Literal::Int(r)) => Ok(Literal::Bool(l == (r as f64))),
              (Literal::Float(l), Literal::Float(r)) => Ok(Literal::Bool(l == r)),
              (Literal::Bool(l), Literal::Bool(r)) => Ok(Literal::Bool(l == r)),
              _ => invalid_operands(&op),
            },
            TokenKind::BangEql => match (left_val, right_val) {
              (Literal::Int(l), Literal::Int(r)) => Ok(Literal::Bool(l != r)),
              (Literal::Int(l), Literal::Float(r)) => Ok(Literal::Bool((l as f64) != r)),
              (Literal::Float(l), Literal::Int(r)) => Ok(Literal::Bool(l != (r as f64))),
              (Literal::Float(l), Literal::Float(r)) => Ok(Literal::Bool(l != r)),
              (Literal::Bool(l), Literal::Bool(r)) => Ok(Literal::Bool(l != r)),
              _ => invalid_operands(&op),
            },
            _ => invalid_operands(&op),
          }
        }
      }
      Expr::Grouping(inner) => eval(ExprTree::x(&inner)),
      Expr::Literal(literal) => Ok(literal.clone()),
    }
  }
}

pub(crate) fn interpret(expr_tree: ExprTree<'_>) {
  match eval(expr_tree) {
    Ok(val) => println!("{:?}", val),
    Err(e) => eprintln!("Error encountered: {}", e),
  }
}

