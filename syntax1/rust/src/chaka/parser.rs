/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                +
 +                                                                                                +
 + Permission is hereby granted, free of charge, to any person obtaining a                        +
 + copy of this software and associated documentation files (the                                  +
 + “Software”), to deal in the Software without restriction, including without                    +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                        +
 + sublicense, and/or sell copies of the Software, and to permit persons to                       +
 + whom the Software is furnished to do so, subject to the following                              +
 + conditions:                                                                                    +
 +                                                                                                +
 + The above copyright notice and this permission notice shall be included                        +
 + in all copies or substantial portions of the Software.                                         +
 +                                                                                                +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                      +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                     +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                        +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                     +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                       +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                     +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                      +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                         +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

use anyhow::anyhow;

use crate::chaka;
use crate::chaka::error::ErrorKind;
use crate::chaka::expr::{Expr, ExprBox, ExprTree};
use crate::chaka::token::{Literal, Token, TokenKind};
use crate::chaka::helpers::cold_path;

type ParseResult<'rt> = anyhow::Result<ExprBox<'rt>>;

pub(crate) struct Parser<'rt> {
  tokens: &'rt Vec<Token<'rt>>,
  current: usize,
  expr_tree_root: Option<ExprBox<'rt>>,
}

impl<'rt> Parser<'rt> {
  pub(crate) fn new(tokens: &'rt Vec<Token<'rt>>) -> Self {
    Self {
      tokens,
      current: 0,
      expr_tree_root: None,
    }
  }
  pub(crate) fn parse(&'rt mut self) -> ExprTree<'rt> {
    // Invalidate the previous tree
    if let Err(_) = self.consume_lexeme(TokenKind::Keyword, "@$", "Expected @$() to contain the expression.") {
      self.expr_tree_root = None;
      // Nobody needs to know that the tree actually exists.
      return ExprTree(None);
    }
    match self.parse_primary() {
      Ok(expr) => {
        self.expr_tree_root = Some(expr);
      }
      Err(_) => {
        // Hide the tree.
        return ExprTree(None);
      }
    };
    // The tree _must_ exist now.
    ExprTree(self.expr_tree_root.as_ref())
  }
  fn parse_expression_descending(&mut self) -> ParseResult<'rt> {
    self.parse_bit_and()
  }
  fn parse_bit_and(&mut self) -> ParseResult<'rt> {
    self.parse_binary_r2l(&[TokenKind::Ampersand], Self::parse_bit_xor)
  }
  // translate the c++ implementation of this function to rust
  fn parse_bit_xor(&mut self) -> ParseResult<'rt> {
    self.parse_binary_r2l(&[TokenKind::Caret], Self::parse_bit_or)
  }
  fn parse_bit_or(&mut self) -> ParseResult<'rt> {
    self.parse_binary_r2l(&[TokenKind::Pipe], Self::parse_equality)
  }
  fn parse_equality(&mut self) -> ParseResult<'rt> {
    self.parse_binary_r2l(&[TokenKind::Eql2, TokenKind::BangEql], Self::parse_comparison)
  }
  fn parse_comparison(&mut self) -> ParseResult<'rt> {
    self.parse_binary_r2l(&[TokenKind::LAngle, TokenKind::RAngle, TokenKind::LAngleEql, TokenKind::RAngleEql], Self::parse_addition)
  }
  fn parse_addition(&mut self) -> ParseResult<'rt> {
    self.parse_binary_r2l(&[TokenKind::Plus, TokenKind::Minus], Self::parse_multiplication)
  }
  fn parse_multiplication(&mut self) -> ParseResult<'rt> {
    self.parse_binary_r2l(&[TokenKind::Star, TokenKind::Slash], Self::parse_bit_shift)
  }
  fn parse_bit_shift(&mut self) -> ParseResult<'rt> {
    self.parse_binary_r2l(&[TokenKind::LAngle2, TokenKind::RAngle2], Self::parse_unary)
  }
  fn parse_binary_r2l(&mut self, kinds: &[TokenKind], parse_next_higher_prec: fn(&mut Parser<'rt>) -> ParseResult<'rt>) -> ParseResult<'rt> {
    let mut base = parse_next_higher_prec(self)?;
    while self.advance_if_match(kinds) {
      // In a binary expression, the left operand should always exist, meaning the op token must always be valid.
      let op = cold_path(|| self.peekb().ok_or(anyhow!("BAD THING: Ill-formed program -- no previous token.")))?;
      // Right recursion
      let right = parse_next_higher_prec(self)?;
      // Modify the base reference
      base = Box::new(Expr::Binary(base, op, right));
    }
    Ok(base)
  }
  fn parse_unary(&mut self) -> ParseResult<'rt> {
    if self.advance_if_match(&[TokenKind::Bang, TokenKind::Minus, TokenKind::Plus, TokenKind::Percent, TokenKind::Tilde]) {
      // The unary operator must always exist.
      let op = cold_path(|| self.peekb().ok_or(anyhow!("BAD THING: Invalid program -- no previous token.")))?;
      let right = self.parse_unary()?;
      Ok(Box::new(Expr::Unary(op, right)))
    } else {
      self.parse_primary()
    }
  }
  fn parse_primary(&mut self) -> ParseResult<'rt> {
    if self.advance_if_match(&[TokenKind::Keyword]) {
      // advance_if_match usually performs a peek -- the resulting token which is guaranteed to exist id the current_ index is okay.
      let matched_token = cold_path(|| self.peekb().ok_or(anyhow!("BAD THING: Invalid program -- no previous token.")))?;
      if matched_token.lexeme == "@F" {
        return Ok(Box::new(Expr::Literal(Literal::Bool(false))));
      }
      if matched_token.lexeme == "@T" {
        return Ok(Box::new(Expr::Literal(Literal::Bool(true))));
      }
    }
    if self.advance_if_match(&[TokenKind::IntLit, TokenKind::FltLit, TokenKind::StrLit]) {
      let matched_token = cold_path(|| self.peekb().ok_or(anyhow!("BAD THING: Invalid program -- no previous token.")))?;
      return Ok(Box::new(Expr::Literal(matched_token.literal)));
    }
    if self.advance_if_match(&[TokenKind::LParen]) {
      let base = self.parse_expression_descending()?;
      self.consume(TokenKind::RParen, "Expecting ')' after the expression!")?;
      return Ok(Box::new(Expr::Grouping(base)));
    }
    Err(self.blame(self.peek(), "Expecting expression!"))
  }

  /******* Helpers *******/

  fn advance_if_match(&mut self, ids: &[TokenKind]) -> bool {
    if !(ids.contains(&self.peek().kind)) {
      return false;
    }
    self.advance();
    true
  }
  fn check(&mut self, id: TokenKind) -> bool {
    if self.is_at_end() {
      return false;
    }
    self.peek().kind == id
  }
  fn advance(&mut self) -> Token<'rt> {
    if !self.is_at_end() {
      self.current += 1;
    }
    // Guaranteed to be valid here.
    self.peekb().unwrap()
  }
  fn is_at_end(&self) -> bool {
    self.peek().kind == TokenKind::Eof
  }
  fn peek(&self) -> Token<'rt> {
    self.tokens[self.current]
  }
  fn peekf(&self) -> Option<Token<'rt>> {
    let next_index = self.current + 1;
    if next_index < self.tokens.len() { Some(self.tokens[next_index]) } else { None }
  }
  fn peekb(&self) -> Option<Token<'rt>> {
    let prev_index = self.current - 1;
    if prev_index > 0 { Some(self.tokens[prev_index]) } else { None }
  }
  fn consume(&mut self, id: TokenKind, msg: &str) -> anyhow::Result<Token<'rt>> {
    if self.check(id) {
      Ok(self.advance())
    } else {
      Err(self.blame(self.peek(), msg))
    }
  }
  fn consume_lexeme(&mut self, id: TokenKind, lexeme: &str, msg: &str) -> anyhow::Result<Token<'rt>> {
    if self.check(id) && self.peek().lexeme == lexeme {
      Ok(self.advance())
    } else {
      Err(self.blame(self.peek(), msg))
    }
  }
  fn blame(&self, token: Token<'rt>, msg: &str) -> anyhow::Error {
    chaka::error::error(
      ErrorKind::Static,
      token.log_info,
      &(if token.kind == TokenKind::Eof
      {
        format!("at end:\n{}", msg)
      } else {
        format!("at '{}':\n{}", token.lexeme, msg)
      }),
    );
    anyhow!("")
  }
  fn synchronize(&mut self) {
    self.advance();
    while !self.is_at_end() {
      if let Some(prev_token) = self.peekb() {
        if prev_token.kind == TokenKind::Semicolon {
          return;
        }
      }
      // @if (@F) {
      // ^
      if self.peek().kind == TokenKind::Keyword {
        return;
      }
      // x: 1
      //  ^
      // OR
      // x / @stack: 1
      // ^ ^ ~~~~~~
      // 1 2 ----> peekf
      if let Some(next_token) = self.peekf() {
        if let TokenKind::Colon | TokenKind::Slash = next_token.kind {
          return;
        }
      }
      self.advance();
    }
  }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod parser_tests_suite {
  use assert_cmd::assert::OutputAssertExt;
  use assert_cmd::Command;
  use predicates::prelude::*;

  use crate::chaka::parser::Parser;
  use crate::chaka::scanner::Scanner;

  #[test]
  fn simple_expression__correct() {
    let mut scanner = Scanner::new("@$(1 + 2 * (3 - 4) / 5.0)");
    let mut parser = Parser::new(&scanner.scan_tokens());
    let result_str = parser.parse().to_string();
    assert_eq!(
      result_str,
      r#"(
 (+ Int(1) (/ (* Int(2) (
  (- Int(3) Int(4))
 )) Float(5)))
)"#
    );
  }

  #[test]
  fn simple_expression__scanner_unrecognised_token__impl() {
    let mut scanner = Scanner::new("@$(1 + 2 * (3 # 4) / 5.0)");
    let mut parser = Parser::new(&scanner.scan_tokens());
    let result_str = parser.parse().to_string();
    assert_eq!(result_str, "<Empty expression node>");
  }

  #[test]
  fn simple_expression__scanner_unrecognised_token() {
    let err_msg = Command::new("cargo")
      .arg("test")
      .arg("simple_expression__scanner_unrecognised_token__impl")
      .arg("--")
      .arg("--nocapture")
      .unwrap();
    err_msg.assert().success().stderr(
      predicate::str::contains(
        r#"[1:14-15] error: Unexpected character.
[1:16-17] error: at '4':
Expecting ')' after the expression!"#
      ));
  }

  #[test]
  fn simple_expression__syntactical_error__impl() {
    let mut scanner = Scanner::new("@$(1 + 2 * (3 - 4) / 5.0");
    let mut parser = Parser::new(&scanner.scan_tokens());
    let result_str = parser.parse().to_string();
    assert_eq!(result_str, "<Empty expression node>");
  }

  #[test]
  fn simple_expression__syntactical_error() {
    let err_msg = Command::new("cargo")
      .arg("test")
      .arg("simple_expression__syntactical_error__impl")
      .arg("--")
      .arg("--nocapture")
      .unwrap();
    err_msg.assert().success().stderr(
      predicate::str::contains(
        r#"[1:21-24] error: at end:
Expecting ')' after the expression!"#
      ));
  }

  #[test]
  fn binary_operator_without_left_operand__impl() {
    let mut scanner = Scanner::new("@$(* 1 2)");
    let mut parser = Parser::new(&scanner.scan_tokens());
    let result_str = parser.parse().to_string();
    assert_eq!(result_str, "<Empty expression node>");
  }

  #[test]
  fn binary_operator_without_left_operand() {
    let err_msg = Command::new("cargo")
      .arg("test")
      .arg("binary_operator_without_left_operand__impl")
      .arg("--")
      .arg("--nocapture")
      .unwrap();
    err_msg.assert().success().stderr(
      predicate::str::contains(
        r#"[1:3-4] error: at '*':
Expecting expression!"#
      ));
  }

  #[test]
  fn binary_operator_without_right_operand__impl() {
    let mut scanner = Scanner::new("@$(1 +)");
    let mut parser = Parser::new(&scanner.scan_tokens());
    let result_str = parser.parse().to_string();
    assert_eq!(result_str, "<Empty expression node>");
  }

  #[test]
  fn binary_operator_without_right_operand() {
    let err_msg = Command::new("cargo")
      .arg("test")
      .arg("binary_operator_without_right_operand__impl")
      .arg("--")
      .arg("--nocapture")
      .unwrap();
    err_msg.assert().success().stderr(
      predicate::str::contains(
        r#"[1:6-7] error: at ')':
Expecting expression!"#
      ));
  }
}
