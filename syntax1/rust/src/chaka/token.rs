/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#[derive(Debug, Clone, Copy)]
pub struct TokenLogInfo {
  pub(crate) line: usize,
  pub(crate) column: usize,
  pub(crate) length: usize,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum TokenKind {
  // Single-char tokens.
  // (
  LParen,
  // )
  RParen,
  // {
  LBrace,
  // }
  RBrace,
  // [
  LSquare,
  // ]
  RSquare,
  // ,
  Comma,
  // .
  Dot,
  // !
  Bang,
  // /
  Slash,
  // :
  Colon,
  // ;
  Semicolon,
  // +
  Plus,
  // -
  Minus,
  // *
  Star,
  // &
  Ampersand,
  // |
  Pipe,
  // ^
  Caret,
  // <
  LAngle,
  // >
  RAngle,
  // ~
  Tilde,
  // %
  Percent,
  // ?
  QMark,
  // Two-char tokens.
  // ->
  RArrow,
  // ==
  Eql2,
  // !=
  BangEql,
  // <=
  LAngleEql,
  // >=
  RAngleEql,
  // <<
  LAngle2,
  // >>
  RAngle2,
  // Multi-char tokens.
  Keyword,
  // [a-zA-Z_][a-zA-Z0-9_]_\$*
  Ident,
  // ".*"
  StrLit,
  // [0-9]+
  IntLit,
  // [0-9]+.[0-9]+
  FltLit,
  // Reserved for internal use.
  Reserved,
  // End of file.
  Eof,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Literal<'rt> {
  MonoState,
  Str(&'rt str),
  Int(i64),
  Float(f64),
  Bool(bool),
}

impl ToString for Literal<'_> {
  fn to_string(&self) -> String {
    match self {
      Literal::MonoState => String::new(),
      Literal::Str(s) => format!("Str({})", s),
      Literal::Int(i) => format!("Int({})", i),
      Literal::Float(f) => format!("Float({})", f),
      Literal::Bool(b) => format!("Bool({})", if *b { "@T" } else { "@F" }),
    }
  }
}

#[derive(Debug, Clone, Copy)]
pub struct Token<'rt> {
  pub kind: TokenKind,
  pub lexeme: &'rt str,
  pub literal: Literal<'rt>,
  pub log_info: TokenLogInfo,
}

impl<'rt> Token<'rt> {
  pub(crate) const fn new(kind: TokenKind, lexeme: &'rt str, literal: Literal<'rt>, log_info: TokenLogInfo) -> Self {
    Self { kind, lexeme, literal, log_info }
  }
}

impl ToString for Token<'_> {
  fn to_string(&self) -> String {
    format!("{:?}({}) = {}", self.kind, self.lexeme, self.literal.to_string())
  }
}
