/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



use std::fs;
use std::io::{self, BufRead, Write};
use std::path::Path;

use crate::chaka::error::{ErrorKind, HAD_ERROR};
use crate::chaka::interpreter;
use crate::chaka::parser::Parser;
use crate::chaka::scanner::Scanner;

pub fn run_file(path: &Path) {
  let source = fs::read_to_string(path).expect("Failed to read file.");
  run(&source);

  if unsafe { HAD_ERROR.contains(ErrorKind::Static as usize) } {
    std::process::exit(65);
  }
  if unsafe { HAD_ERROR.contains(ErrorKind::Runtime as usize) } {
    std::process::exit(70);
  }
}

pub fn run_prompt() {
  let intro = format!(
    "Chaka {}.{}.{}/Rust (C) Shivam \"Vivraan\" Mukherjee",
    env!("CARGO_PKG_VERSION_MAJOR"),
    env!("CARGO_PKG_VERSION_MINOR"),
    env!("CARGO_PKG_VERSION_PATCH"));
  let bar = "=".repeat(intro.len());
  print!("{}\n{}\n", intro, bar);

  loop {
    print!("> ");
    io::stdout().flush().unwrap();
    let stdin = io::stdin();
    let mut line_raw = String::new();
    stdin.lock().read_line(&mut line_raw).unwrap();

    let line = line_raw.trim();
    if line.is_empty() {
      break;
    }
    run(&line);
    unsafe { HAD_ERROR.clear(); }
  }
}

pub fn run(source: &str) {
  let mut scanner = Scanner::new(source);
  let tokens = scanner.scan_tokens();

  let mut parser = Parser::new(tokens);
  let expr_tree = parser.parse();

  if unsafe { HAD_ERROR.contains(ErrorKind::Static as usize) } {
    return;
  }

  interpreter::interpret(expr_tree);
}