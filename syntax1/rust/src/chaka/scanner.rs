/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                +
 +                                                                                                +
 + Permission is hereby granted, free of charge, to any person obtaining a                        +
 + copy of this software and associated documentation files (the                                  +
 + “Software”), to deal in the Software without restriction, including without                    +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                        +
 + sublicense, and/or sell copies of the Software, and to permit persons to                       +
 + whom the Software is furnished to do so, subject to the following                              +
 + conditions:                                                                                    +
 +                                                                                                +
 + The above copyright notice and this permission notice shall be included                        +
 + in all copies or substantial portions of the Software.                                         +
 +                                                                                                +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                      +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                     +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                        +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                     +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                       +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                     +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                      +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                         +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

use std::collections::HashSet;

use crate::chaka::error::{error, ErrorKind};
use crate::chaka::token::{Literal, Token, TokenKind, TokenLogInfo};

pub struct Scanner<'rt> {
  source: &'rt str,
  tokens: Vec<Token<'rt>>,
  strings: HashSet<String>,
  start: usize,
  current: usize,
  token_log_info: TokenLogInfo,
}

impl<'rt> Scanner<'rt> {
  pub fn new(source: &'rt str) -> Self {
    Self {
      source,
      tokens: Vec::new(),
      strings: HashSet::new(),
      start: 0,
      current: 0,
      token_log_info: TokenLogInfo {
        line: 1,
        column: 0,
        length: 0,
      },
    }
  }

  pub fn scan_tokens(&mut self) -> &Vec<Token<'rt>> {
    while !self.is_at_end() {
      self.start = self.current;
      self.scan_token();
    }

    self.tokens.push(Token::new(TokenKind::Eof, "", Literal::MonoState, self.token_log_info));
    &self.tokens
  }
  fn scan_token(&mut self) {
    let c = self.advance();

    match c {
      ' ' | '\r' | '\t' => {} // Ignore whitespace
      '\n' => {
        self.new_line();
      }
      '(' => self.take_token(TokenKind::LParen),
      ')' => self.take_token(TokenKind::RParen),
      '{' => self.take_token(TokenKind::LBrace),
      '}' => self.take_token(TokenKind::RBrace),
      '[' => self.take_token(TokenKind::LSquare),
      ']' => self.take_token(TokenKind::RSquare),
      ',' => self.take_token(TokenKind::Comma),
      '.' => self.take_token(TokenKind::Dot),
      ':' => self.take_token(TokenKind::Colon),
      ';' => self.take_token(TokenKind::Semicolon),
      '&' => self.take_token(TokenKind::Ampersand),
      '|' => self.take_token(TokenKind::Pipe),
      '^' => self.take_token(TokenKind::Caret),
      '+' => self.take_token(TokenKind::Plus),
      '*' => self.take_token(TokenKind::Star),
      '~' => self.take_token(TokenKind::Tilde),
      '%' => self.take_token(TokenKind::Percent),
      '!' => if self.check_advance('=') {
        self.take_token(TokenKind::BangEql)
      } else {
        self.take_token(TokenKind::Bang)
      },
      '-' => if self.check_advance('>') {
        self.take_token(TokenKind::RArrow)
      } else {
        self.take_token(TokenKind::Minus)
      },
      '/' => if self.check_advance('/') {
        while self.peek() != '\n' && !self.is_at_end() {
          self.advance();
        }
      } else {
        self.take_token(TokenKind::Slash)
      },
      '@' => if self.peek().is_ident_alnumsym() {
        self.take_terminal(TokenKind::Keyword)
      },
      '"' => self.take_string(),
      '<' => if self.check_advance('=') {
        self.take_token(TokenKind::LAngleEql)
      } else if self.check_advance('<') {
        self.take_token(TokenKind::LAngle2)
      } else {
        self.take_token(TokenKind::LAngle)
      },
      '>' => if self.check_advance('=') {
        self.take_token(TokenKind::RAngleEql)
      } else if self.check_advance('>') {
        self.take_token(TokenKind::RAngle2)
      } else {
        self.take_token(TokenKind::RAngle)
      },
      '=' => if self.check_advance('=') {
        self.take_token(TokenKind::Eql2)
      } else {
        self.take_token(TokenKind::Reserved)
      },
      _ => {
        if c.is_ascii_digit() {
          self.take_number();
        } else if c.is_ident_alnumsym() {
          self.take_terminal(TokenKind::Ident);
        } else { // Catch-all for invalid characters
          self.update_log_info(self.selected_source());
          error(ErrorKind::Static, self.token_log_info, "Unexpected character.");
        }
      }
    }
  }
  fn take_string(&mut self) {
    while self.peek() != '"' && !self.is_at_end() {
      if self.peek() == '\n' {
        self.new_line();
      }
      self.advance();
    }
    if self.is_at_end() {
      error(ErrorKind::Static, self.token_log_info, "Unterminated string.");
    }
    // Consume the closing '"'
    self.advance();
    // Trim the surrounding quotes
    let value = &self.source[self.start + 1..self.current - 1];
    self.take_literal_token(TokenKind::StrLit, Literal::Str(value));
    self.strings.insert(value.to_string());
  }
  // Just base-10 for now.
  fn take_number(&mut self) {
    while self.peek().is_ascii_digit() {
      self.advance();
    }
    if self.peek() == '.' {
      if self.peekf().is_ascii_digit() {
        // Consume the "."
        self.advance();
        while self.peek().is_ascii_digit() {
          self.advance();
        }
      }
    } else {
      // It's just an int.
      let token_text = self.selected_source();
      match token_text.parse::<i64>() {
        Ok(value) => {
          self.take_literal_token(TokenKind::IntLit, Literal::Int(value));
        }
        Err(_) => {
          self.update_log_info(token_text);
          error(ErrorKind::Static, self.token_log_info, "Invalid integer literal.");
        }
      }
      return;
    }
    // It's a float.
    let token_text = self.selected_source();
    match token_text.parse::<f64>() {
      Ok(value) => {
        self.take_literal_token(TokenKind::FltLit, Literal::Float(value));
      }
      Err(_) => {
        self.update_log_info(token_text);
        error(ErrorKind::Static, self.token_log_info, "Invalid floating-point literal.");
      }
    }
  }
  fn take_terminal(&mut self, token_id: TokenKind) {
    assert!(token_id == TokenKind::Ident || token_id == TokenKind::Keyword);
    while self.peek().is_ident_alnumsym() {
      self.advance();
    }
    self.take_token(token_id);
  }
  fn take_token(&mut self, token_id: TokenKind) {
    let token_text = self.selected_source();
    self.update_log_info(token_text);
    self.tokens.push(Token::new(token_id, token_text, Literal::MonoState, self.token_log_info));
  }
  fn take_literal_token(&mut self, token_id: TokenKind, literal: Literal<'rt>) {
    let token_text = self.selected_source();
    self.update_log_info(token_text);
    self.tokens.push(Token::new(token_id, token_text, literal, self.token_log_info));
  }
  fn advance(&mut self) -> char {
    self.current += 1;
    self.source.chars().nth(self.current - 1).unwrap()
  }
  fn check_advance(&mut self, expected: char) -> bool {
    if self.is_at_end() {
      return false;
    }
    if self.source.chars().nth(self.current).unwrap() != expected {
      return false;
    }
    self.current += 1;
    true
  }
  fn peek(&self) -> char {
    if self.is_at_end() {
      return '\0';
    }
    self.source.chars().nth(self.current).unwrap()
  }
  fn peekf(&self) -> char {
    if self.current + 1 >= self.source.len() {
      return '\0';
    }
    self.source.chars().nth(self.current + 1).unwrap()
  }
  fn is_at_end(&self) -> bool {
    self.current >= self.source.len()
  }
  fn update_log_info(&mut self, token_text: &str) {
    self.token_log_info.column = self.start;
    self.token_log_info.length = token_text.len();
  }
  fn selected_source(&self) -> &'rt str {
    &self.source[self.start..self.current]
  }
  fn new_line(&mut self) {
    self.token_log_info.line += 1;
    self.token_log_info.column = 0;
  }
}

trait IdentAlNumSym {
  fn is_ident_alnumsym(&self) -> bool;
}

impl IdentAlNumSym for char {
  fn is_ident_alnumsym(&self) -> bool {
    self.is_alphanumeric() || *self == '_' || *self == '$'
  }
}


#[cfg(test)]
mod scanner_tests_suite {
  use crate::chaka::scanner::Scanner;
  use crate::chaka::token::TokenKind;

  #[test]
  fn random_chars() {
    let source_to_test =
      r#"// this is a comment
             (( )){} // grouping stuff
             &|^;:!*+-/=< > -> <= == // operators"#;

    let test_token_ids = vec![
      TokenKind::LParen,
      TokenKind::LParen,
      TokenKind::RParen,
      TokenKind::RParen,
      TokenKind::LBrace,
      TokenKind::RBrace,
      //
      TokenKind::Ampersand,
      TokenKind::Pipe,
      TokenKind::Caret,
      TokenKind::Semicolon,
      TokenKind::Colon,
      TokenKind::Bang,
      TokenKind::Star,
      TokenKind::Plus,
      TokenKind::Minus,
      TokenKind::Slash,
      TokenKind::Reserved,
      TokenKind::LAngle,
      TokenKind::RAngle,
      TokenKind::RArrow,
      TokenKind::LAngleEql,
      TokenKind::Eql2,
      //
      TokenKind::Eof,
    ];

    let mut scanner = Scanner::new(source_to_test);
    let tokens = scanner.scan_tokens();

    for (test_token_id, token) in test_token_ids.iter().zip(tokens.iter()) {
      assert_eq!(test_token_id, &token.kind);
    }
  }

  #[test]
  fn program() {
    let source_to_test = r#"
make_hot_tea: @scope() {
	@result: @scope() -> temp_celsius, temp_kelvin {
		temp_celsius/@stack: 40;
		temp_kelvin/@stack: temp_celsius + 273.15;
	}
}

tea: make_hot_tea();
// tea: make_hot_tea().@result;

print(tea);"#;

    let test_token_ids = vec![
      //
      TokenKind::Ident,
      TokenKind::Colon,
      TokenKind::Keyword,
      TokenKind::LParen,
      TokenKind::RParen,
      TokenKind::LBrace,
      //
      TokenKind::Keyword,
      TokenKind::Colon,
      TokenKind::Keyword,
      TokenKind::LParen,
      TokenKind::RParen,
      TokenKind::RArrow,
      TokenKind::Ident,
      TokenKind::Comma,
      TokenKind::Ident,
      TokenKind::LBrace,
      //
      TokenKind::Ident,
      TokenKind::Slash,
      TokenKind::Keyword,
      TokenKind::Colon,
      TokenKind::IntLit,
      TokenKind::Semicolon,
      //
      TokenKind::Ident,
      TokenKind::Slash,
      TokenKind::Keyword,
      TokenKind::Colon,
      TokenKind::Ident,
      TokenKind::Plus,
      TokenKind::FltLit,
      TokenKind::Semicolon,
      //
      TokenKind::RBrace,
      //
      TokenKind::RBrace,
      //
      TokenKind::Ident,
      TokenKind::Colon,
      TokenKind::Ident,
      TokenKind::LParen,
      TokenKind::RParen,
      TokenKind::Semicolon,
      //
      TokenKind::Ident,
      TokenKind::LParen,
      TokenKind::Ident,
      TokenKind::RParen,
      TokenKind::Semicolon,
      //
      TokenKind::Eof,
    ];

    let mut scanner = Scanner::new(source_to_test);
    let tokens = scanner.scan_tokens();

    for (test_token_id, token) in test_token_ids.iter().zip(tokens.iter()) {
      assert_eq!(test_token_id, &token.kind,
                 "Test token id: {:?}, Scanned token id: {:?}",
                 test_token_ids,
                 tokens.iter().map(|token| token.kind).collect::<Vec<TokenKind>>());
    }
  }
}


