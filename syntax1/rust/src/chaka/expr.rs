/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023-2024. Shivam "Vivraan" Mukherjee.                                           +
 +                                                                                                +
 + Permission is hereby granted, free of charge, to any person obtaining a                        +
 + copy of this software and associated documentation files (the                                  +
 + “Software”), to deal in the Software without restriction, including without                    +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                        +
 + sublicense, and/or sell copies of the Software, and to permit persons to                       +
 + whom the Software is furnished to do so, subject to the following                              +
 + conditions:                                                                                    +
 +                                                                                                +
 + The above copyright notice and this permission notice shall be included                        +
 + in all copies or substantial portions of the Software.                                         +
 +                                                                                                +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                      +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                     +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                        +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                     +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                       +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                     +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                      +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                         +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

use std::cell::RefCell;
use std::fmt::Display;

use crate::chaka::token::{Literal, Token};

// The concepts of ownership and borrowing are fairly standardised in Rust so I don't need to annotate types with that info.
pub(crate) type ExprBox<'rt> = Box<Expr<'rt>>;
type ExprRef<'rt> = &'rt Expr<'rt>;

static GROUP_PH: &str = "<group>";

// Welcome to Rust.
pub enum Expr<'rt> {
  Binary(ExprBox<'rt>, Token<'rt>, ExprBox<'rt>),
  Grouping(ExprBox<'rt>),
  Literal(Literal<'rt>),
  Unary(Token<'rt>, ExprBox<'rt>),
}

impl Expr<'_> {
  fn surround(&self, name: &str, exprs: &[ExprRef<'_>], num_groups: usize) -> String {
    let mut ng = num_groups;
    let mut subexprs_str = exprs[0].to_string_impl(ng);
    if let Expr::Grouping(_) = exprs[0] {
      ng += 1;
    }
    for expr in &exprs[1..] {
      subexprs_str.push(' ');
      subexprs_str.push_str(&expr.to_string_impl(ng).as_str());
      if let Expr::Grouping(_) = expr {
        ng += 1;
      }
    }
    let start_ws = if name != GROUP_PH { " ".into() } else { format!("\n{}", " ".repeat(ng)) };
    let end_ws = if name != GROUP_PH { "".into() } else { format!("\n{}", " ".repeat(ng - 1)) };
    format!("({}{}{}{})", if name == GROUP_PH { "" } else { name }, start_ws, subexprs_str, end_ws)
  }
  fn to_string_impl(&self, num_groups: usize) -> String {
    thread_local! {
            static NG: RefCell<usize> = RefCell::new(0);
        }
    NG.with(|ng| {
      unsafe {
        let one_true_ng = ng.as_ptr();
        *one_true_ng = num_groups;
        match self {
          Self::Binary(left, op, right) =>
            self.surround(&op.lexeme, &[&*left, &*right], *one_true_ng),
          Self::Grouping(inner) => {
            *one_true_ng += 1;
            self.surround(GROUP_PH, &[&*inner], *one_true_ng)
          }
          Self::Literal(literal) =>
            literal.to_string(),
          Self::Unary(op, right) =>
            self.surround(&op.lexeme, &[&*right], *one_true_ng),
        }
      }
    })
  }
}

impl Display for Expr<'_> {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", self.to_string_impl(0))
  }
}

pub struct ExprTree<'rt>(pub Option<&'rt ExprBox<'rt>>);

impl Display for ExprTree<'_> {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    let str = match self.0 {
      Some(expr) => expr.to_string(),
      None => "<Empty expression node>".to_string(),
    };
    write!(f, "{}", str)
  }
}

impl<'rt> ExprTree<'rt> {
  pub(crate) fn x(expr: &'rt ExprBox<'rt>) -> Self {
    Self(Some(expr))
  }
}
