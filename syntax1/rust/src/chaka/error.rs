/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

use bit_set::BitSet;
use once_cell::unsync::Lazy;
use variant_count::VariantCount;

use crate::chaka::token::TokenLogInfo;

#[derive(VariantCount)]
pub(crate) enum ErrorKind { Other = 0b00, Static = 0b01, Runtime = 0b10 }

pub(crate) static mut HAD_ERROR: Lazy<BitSet, fn() -> BitSet> = Lazy::new(|| BitSet::with_capacity(ErrorKind::VARIANT_COUNT));

pub(crate) fn error(kind: ErrorKind, info: TokenLogInfo, message: &str) {
  if info.length > 1 {
    eprintln!("[{}:{}-{}] error: {}", info.line, info.column, info.column + info.length - 1, message);
  } else {
    eprintln!("[{}:{}] error: {}", info.line, info.column, message);
  }
  unsafe { HAD_ERROR.insert(kind as usize); }
}

