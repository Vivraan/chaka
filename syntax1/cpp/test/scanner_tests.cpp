/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 17-06-2023.
//

#include <array>
#include <numeric>
#include "gtest/gtest.h"
#include "chaka/token.h"
#include "chaka/scanner.h"

using namespace chaka::scanner;
using chaka::token::TokenKind;

TEST(ScannerTestsSuite, RandomChars) {
  constexpr std::string_view source_to_test =
    "// this is a comment\n"
    "(( )){} // grouping stuff\n"
    "&|^;:!*+-/=< > -> <= == // operators";

  // Requires the type for TokenKind, otherwise it assumes it to be TokenKind::_enumerated
  // This version deduces the number of arguments
  constexpr auto test_token_ids =
    std::to_array<TokenKind>({
      TokenKind::LParen,
      TokenKind::LParen,
      TokenKind::RParen,
      TokenKind::RParen,
      TokenKind::LBrace,
      TokenKind::RBrace,
      //
      TokenKind::Ampersand,
      TokenKind::Pipe,
      TokenKind::Caret,
      TokenKind::Semicolon,
      TokenKind::Colon,
      TokenKind::Bang,
      TokenKind::Star,
      TokenKind::Plus,
      TokenKind::Minus,
      TokenKind::Slash,
      TokenKind::Reserved,
      TokenKind::LAngle,
      TokenKind::RAngle,
      TokenKind::RArrow,
      TokenKind::LAngleEql,
      TokenKind::Eql2,
      ////
      TokenKind::Eof
    });

  Scanner scanner{source_to_test};
  const auto tokens = scanner.scan_tokens();

  for (auto i = 0; i < tokens.size(); i++) {
    EXPECT_EQ(test_token_ids[i], tokens[i].kind);
  }
}

TEST(ScannerTestsSuite, Program) {
  constexpr std::string_view source_to_test =
    R"(
make_hot_tea: @scope() {
	@result: @scope() -> temp_celsius, temp_kelvin {
		temp_celsius/@stack: 40;
		temp_kelvin/@stack: temp_celsius + 273.15;
	}
}

tea: make_hot_tea();
// tea: make_hot_tea().@result;

print(tea);)";

  auto test_token_ids = std::to_array<TokenKind>(
    {
      TokenKind::Ident,
      TokenKind::Colon,
      TokenKind::Keyword,
      TokenKind::LParen,
      TokenKind::RParen,
      TokenKind::LBrace,
      //
      TokenKind::Keyword,
      TokenKind::Colon,
      TokenKind::Keyword,
      TokenKind::LParen,
      TokenKind::RParen,
      TokenKind::RArrow,
      TokenKind::Ident,
      TokenKind::Comma,
      TokenKind::Ident,
      TokenKind::LBrace,
      //
      TokenKind::Ident,
      TokenKind::Slash,
      TokenKind::Keyword,
      TokenKind::Colon,
      TokenKind::IntLit,
      TokenKind::Semicolon,
      //
      TokenKind::Ident,
      TokenKind::Slash,
      TokenKind::Keyword,
      TokenKind::Colon,
      TokenKind::Ident,
      TokenKind::Plus,
      TokenKind::FltLit,
      TokenKind::Semicolon,
      //
      TokenKind::RBrace,
      //
      TokenKind::RBrace,
      //
      TokenKind::Ident,
      TokenKind::Colon,
      TokenKind::Ident,
      TokenKind::LParen,
      TokenKind::RParen,
      TokenKind::Semicolon,
      //
      TokenKind::Ident,
      TokenKind::LParen,
      TokenKind::Ident,
      TokenKind::RParen,
      TokenKind::Semicolon,
      //
      TokenKind::Eof
    });

  Scanner scanner{source_to_test};
  auto tokens = scanner.scan_tokens();

  for (auto i = 0; i < tokens.size(); i++) {
    EXPECT_EQ(test_token_ids[i], tokens[i].kind)
            << "Test Tokens: "
            << std::accumulate(
          std::next(test_token_ids.begin()), test_token_ids.end(),
          std::string{test_token_ids[0]._to_string()},
          [](const auto& acc, const auto& token) {
            return acc + ", " + token._to_string();
          })
            << '\n'
            << "Scanned Tokens: "
            << std::accumulate(
          std::next(tokens.begin()), tokens.end(),
          std::string{tokens[0].kind._to_string()},
          [](const auto& acc, const auto& token) {
            return acc + ", " + token.kind._to_string();
          })
            << '\n';
  }
}
