/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 09-08-2023.
//

#ifndef CHAKA_CPP_ROOT_SRC_CHAKA_PARSER_H_
#define CHAKA_CPP_ROOT_SRC_CHAKA_PARSER_H_

#include <gsl/gsl>

#include <expected>
#include <optional>
#include <ranges>
#include <vector>

#include "error.h"
#include "expr.h"
#include "stmt.h"
#include "token.h"

namespace chaka {
namespace parser {
using token::Token;
using token::TokenKind;
using expr::ExprOwningPtr;
using stmt::StmtOwningPtr;

class Parser final {
public:
  explicit Parser(const std::span<Token> tokens) : tokens_{tokens} {}

  using ParseFunc = Result<ExprOwningPtr> (Parser::*)() noexcept;

  std::vector<StmtOwningPtr> parse() noexcept;
  Result<StmtOwningPtr> parse_declaration() noexcept;
  Result<StmtOwningPtr> parse_var_def(const Token& name) noexcept;
  Result<StmtOwningPtr> parse_statement() noexcept;
  Result<StmtOwningPtr> parse_expression_statement() noexcept;
  Result<std::vector<StmtOwningPtr>> parse_block() noexcept;
  Result<StmtOwningPtr> parse_dbg_log_statement() noexcept;
  Result<ExprOwningPtr> parse_expression() noexcept;

private:
  Result<ExprOwningPtr> parse_expression_internal() noexcept;
  Result<ExprOwningPtr> parse_assignment() noexcept;
  Result<ExprOwningPtr> parse_bit_and() noexcept;
  Result<ExprOwningPtr> parse_bit_xor() noexcept;
  Result<ExprOwningPtr> parse_bit_or() noexcept;
  Result<ExprOwningPtr> parse_equality() noexcept;
  Result<ExprOwningPtr> parse_comparison() noexcept;
  Result<ExprOwningPtr> parse_addition() noexcept;
  Result<ExprOwningPtr> parse_multiplication() noexcept;
  Result<ExprOwningPtr> parse_bit_shift() noexcept;
  Result<ExprOwningPtr> parse_binary_r2l(
    std::initializer_list<TokenKind> kinds,
    ParseFunc parse_next_higher_prec
  ) noexcept;
  Result<ExprOwningPtr> parse_unary() noexcept;
  Result<ExprOwningPtr> parse_primary() noexcept;

  /******* Helpers *******/
  bool advance_if_match(const std::initializer_list<TokenKind>& kinds_to_match) noexcept;
  bool check(const TokenKind& token_id) const noexcept;
  bool is_at_end() const noexcept;
  Token peek() const noexcept;
  std::optional<Token> peekf() const noexcept;
  std::optional<Token> advance() noexcept;
  std::optional<Token> peekb() const noexcept;
  Result<std::optional<Token>> consume(TokenKind kind, std::string_view message) noexcept;
  Result<std::optional<Token>> consume_lexeme(
    const TokenKind& kind, std::string_view lexeme, std::string_view message) noexcept;
  static std::errc blame(Token token, std::string_view msg) noexcept;
  void synchronize() noexcept;

  std::span<Token> tokens_;
  gsl::index current_ = 0;
};
} // namespace parser
} // namespace chaka

#endif //CHAKA_CPP_ROOT_SRC_CHAKA_PARSER_H_
