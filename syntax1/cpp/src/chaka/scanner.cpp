/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 04-06-2023.
//

#include "scanner.h"

#include <cassert>
#include <charconv>
#include <iostream>
#include <set>

#include "error.h"

namespace chaka {

namespace scanner {

using token::Token;

/***** lex *****/
std::span<Token> Scanner::scan_tokens() noexcept {
  // We are at the beginning of the next lexeme.
  while (!is_at_end()) {
    start_ = current_;
    scan_token();
  }
  // Not the sentinel Token as it needs the log info.
  tokens_.emplace_back(TokenKind::Eof, "", Literal{std::monostate{}}, token_log_info_);
  return tokens_;
}

void Scanner::scan_token() noexcept {
  const char c = advance();
  switch (c) {
    case ' ':
    case '\r':
    case '\t':
      // Ignore whitespace.
      break;
    case '\n':
      new_line();
      break;
    case '(':
      take_token(TokenKind::LParen);
      break;
    case ')':
      take_token(TokenKind::RParen);
      break;
    case '{':
      take_token(TokenKind::LBrace);
      break;
    case '}':
      take_token(TokenKind::RBrace);
      break;
    case '[':
      take_token(TokenKind::LSquare);
      break;
    case ']':
      take_token(TokenKind::RSquare);
      break;
    case ',':
      take_token(TokenKind::Comma);
      break;
    case '.':
      take_token(TokenKind::Dot);
      break;
    case ':':
      take_token(TokenKind::Colon);
      break;
    case ';':
      take_token(TokenKind::Semicolon);
      break;
    case '&':
      take_token(TokenKind::Ampersand);
      break;
    case '|':
      take_token(TokenKind::Pipe);
      break;
    case '^':
      take_token(TokenKind::Caret);
      break;
    case '+':
      take_token(TokenKind::Plus);
      break;
    case '*':
      take_token(TokenKind::Star);
      break;
    case '~':
      take_token(TokenKind::Tilde);
      break;
    case '%':
      take_token(TokenKind::Percent);
      break;
    case '?':
      take_token(TokenKind::QMark);
      break;
    case '!':
      if (check_advance('=')) {
        take_token(TokenKind::BangEql);
      } else {
        take_token(TokenKind::Bang);
      }
      break;
    case '-':
      if (check_advance('>')) {
        take_token(TokenKind::RArrow);
      } else {
        take_token(TokenKind::Minus);
      }
      break;
    case '/':
      if (check_advance('/')) {
        // A comment goes on until the end of the line.
        while (peek() != '\n' && !is_at_end()) {
          advance();
        }
      } else {
        take_token(TokenKind::Slash);
      }
      break;
    case '@':
      if (is_ident_alnumsym(peek())) {
        take_terminal(TokenKind::Keyword);
      }
      break;
    case '"':
      take_string();
      break;
    case '<':
      if (check_advance('<')) {
        take_token(TokenKind::LAngle2);
      } else if (check_advance('=')) {
        take_token(TokenKind::LAngleEql);
      } else {
        take_token(TokenKind::LAngle);
      }
      break;
    case '>':
      if (check_advance('>')) {
        take_token(TokenKind::RAngle2);
      } else if (check_advance('=')) {
        take_token(TokenKind::RAngleEql);
      } else {
        take_token(TokenKind::RAngle);
      }
      break;
    case '=':
      if (check_advance('=')) {
        take_token(TokenKind::Eql2);
      } else {
        take_token(TokenKind::Reserved);
      }
      break;
    default:
      if (std::isdigit(c)) {
        take_number();
      } else if (is_ident_alnumsym(c)) {
        take_terminal(TokenKind::Ident);
      } else { // catch-all for invalid characters
        update_log_info(selected_source());
        error(ErrorKind::Static, ErrorFatality::Fatal, token_log_info_, "Unexpected character.");
      }
      break;
  }
}

/**** helpers *****/

void Scanner::take_string() noexcept {
  while (peek() != '"' && !is_at_end()) {
    if (peek() != '\n') {
      new_line();
    }
    advance();
  }
  if (is_at_end()) {
    update_log_info(selected_source());
    error(ErrorKind::Static, ErrorFatality::Fatal, token_log_info_, "Unterminated string.");
    return;
  }
  // Consume the closing '"'.
  advance();
  // Trim the surrounding quotes.
  const auto str_start = start_ + 1;
  const auto str_end = current_ - 1;
  auto value = source_.substr(str_start, str_end - str_start);
  take_token(TokenKind::StrLit, Literal{value});
  // Pool the value as a new string if it's unique.
  strings_.emplace(value);
}

// Just base-10 for now.
void Scanner::take_number() noexcept {
  while (std::isdigit(peek())) {
    advance();
  }
  if (peek() == '.') {
    // Look for a fractional part.
    if (std::isdigit(peekf())) {
      // Consume the '.'
      advance();
      while (std::isdigit(peek())) {
        advance();
      }
    }
  } else {
    // It's just an int.
    const auto token_text = selected_source();
    i64 value;
    const auto start = token_text.data();
    const auto end = start + token_text.size();
    const auto [ptr, ec] = std::from_chars(start, end, value);
    if (constexpr auto ok = std::errc{}; ec == ok) {
      take_token(TokenKind::IntLit, Literal{value});
    } else {
      update_log_info(token_text);
      error(ErrorKind::Static, ErrorFatality::Fatal, token_log_info_, "Invalid integer literal.");
    }
    return;
  }
  // It's a float.
  const auto token_text = selected_source();
  f64 value;
  const auto start = token_text.data();
  const auto end = start + token_text.size();
  const auto [ptr, ec] = std::from_chars(start, end, value);
  if (constexpr auto ok = std::errc{}; ec == ok) {
    take_token(TokenKind::FltLit, Literal{value});
  } else {
    update_log_info(token_text);
    error(ErrorKind::Static, ErrorFatality::Fatal, token_log_info_, "Invalid floating-point literal.");
  }
}

void Scanner::take_terminal(const TokenKind token_id) noexcept {
  {
    const auto token_id_cmp = token_id._value;
    assert((token_id_cmp == TokenKind::Keyword || token_id_cmp == TokenKind::Ident));
  }
  while (is_ident_alnumsym(peek())) {
    advance();
  }
  take_token(token_id);
}

void Scanner::take_token(TokenKind token_id, Literal literal) noexcept {
  auto token_text = selected_source();
  update_log_info(token_text);
  tokens_.emplace_back(token_id, token_text, literal, token_log_info_);
}

char Scanner::advance() noexcept {
  return source_[current_++];
}

bool Scanner::check_advance(char expected) noexcept {
  if (is_at_end()) {
    return false;
  }
  if (source_[current_] != expected) {
    return false;
  }
  current_++;
  return true;
}

char Scanner::peek() const noexcept {
  if (is_at_end()) {
    return '\0';
  }
  return source_[current_];
}

char Scanner::peekf() const noexcept {
  if (current_ + 1 >= source_.size()) {
    return '\0';
  }
  return source_[current_ + 1];
}

bool Scanner::is_at_end() const noexcept {
  return current_ >= source_.size();
}

void Scanner::update_log_info(std::string_view token_text) noexcept {
  token_log_info_.column = start_;
  token_log_info_.length = token_text.size();
}

std::string_view Scanner::selected_source() const noexcept {
  return source_.substr(start_, current_ - start_);
}

void Scanner::new_line() noexcept {
  token_log_info_.line++;
  token_log_info_.column = 0;
}

bool is_ident_alnumsym(const char c) noexcept {
  return std::isalnum(c) || c == '_' || c == '$';
}

} // namespace scanner

} // namespace chaka
