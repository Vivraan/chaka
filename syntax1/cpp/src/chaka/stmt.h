/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 13-10-2023.
//

#ifndef CHAKA_CPP_ROOT_SRC_CHAKA_STMT_H_
#define CHAKA_CPP_ROOT_SRC_CHAKA_STMT_H_

#include <variant>
#include <vector>

#include "expr.h"

namespace chaka {
namespace stmt {
using token::Token;
using expr::ExprOwningPtr;

struct DbgLog;
struct VarDef;
struct Block;

struct Stmt;
namespace {
using StmtOwningPtr = std::unique_ptr<const Stmt>;
using StmtViewingPtr = const Stmt* const;
}

struct DbgLog final {
  ExprOwningPtr expr;
};

struct VarDef final {
  Token name;
  ExprOwningPtr initializer;
};

struct Block final {
  std::vector<StmtOwningPtr> statements;
};

struct Stmt : public std::variant<ExprOwningPtr, DbgLog, VarDef, Block> {
  using OwningPtr = StmtOwningPtr;
  using ViewingPtr = StmtViewingPtr;
};

constexpr StmtViewingPtr view(const StmtOwningPtr& stmt) noexcept;

static_assert(DiscUnionBasedNode<Stmt, ExprOwningPtr, DbgLog, VarDef, Block>);

StmtOwningPtr make_dbg_log(ExprOwningPtr expr) noexcept;

StmtOwningPtr make_expression(ExprOwningPtr expr) noexcept;

StmtOwningPtr make_var_def(Token name, ExprOwningPtr initializer) noexcept;

StmtOwningPtr make_block(std::vector<StmtOwningPtr>&& statements) noexcept;

token::TokenLogInfo get_log_info(StmtViewingPtr stmt_tree_ptr) noexcept;


} // namespace stmt
} // namespace chaka

#endif //CHAKA_CPP_ROOT_SRC_CHAKA_STMT_H_
