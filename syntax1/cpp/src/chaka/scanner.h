/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 04-06-2023.
//

#ifndef CHAKA_SRC_SCANNER_H_
#define CHAKA_SRC_SCANNER_H_

#include <ranges>
#include <set>
#include <vector>

#include "token.h"

namespace chaka {

namespace scanner {

using token::Token;
using token::TokenKind;

class Scanner final {
 public:
  explicit Scanner(std::string_view source) : source_(source) {};
  std::span<Token> scan_tokens() noexcept;

 private:
  /****** lex *****/
  void scan_token() noexcept;

  /***** helpers *****/
  void take_string() noexcept;
  void take_number() noexcept;
  void take_terminal(const TokenKind token_id) noexcept;
  void take_token(TokenKind token_id, Literal literal = Literal{std::monostate{}}) noexcept;
  char advance() noexcept;
  bool check_advance(char expected) noexcept;
  char peek() const noexcept;
  char peekf() const noexcept;
  bool is_at_end() const noexcept;
  void update_log_info(std::string_view token_text) noexcept;

  std::string_view selected_source() const noexcept;
  void new_line() noexcept;

  const std::string_view source_;
  std::vector<Token> tokens_;
  std::set<std::string> strings_;

  size_t start_ = 0, current_ = 0;
  token::TokenLogInfo token_log_info_{1, 0, 0};
};

bool is_ident_alnumsym(const char c) noexcept;

} // namespace scanner

} // namespace chaka

#endif //CHAKA_SRC_SCANNER_H_
