/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 15-12-2023.
//

#ifndef CHAKA_CPP_ROOT_SRC_CHAKA_ENV_H_
#define CHAKA_CPP_ROOT_SRC_CHAKA_ENV_H_

#include <unordered_map>

#include "error.h"
#include "token.h"

namespace chaka::env {
using token::Literal;

class Env final {
public:
  Env() noexcept = default;
  explicit Env(const std::shared_ptr<Env>& enclosing_env) noexcept : enclosing_env{enclosing_env} {}
  Env(const Env&) = delete;
  Env& operator=(const Env&) = delete;
  Env(Env&&) = default;
  Env& operator=(Env&&) = default;
  ~Env() = default;

  bool define(const std::string_view name, const Literal& value) noexcept {
    const std::string key{name};
    // Redefine the outermost variable if it already exists.
    if (enclosing_env != nullptr && enclosing_env->values_.contains(key)) {
      return enclosing_env->define(name, value);
    }
    // Variable is local to this scope.
    values_[key] = value;
    return true;
  }

  Result<Literal> get(const std::string_view name) {
    if (const std::string key{name}; values_.contains(key) && !std::holds_alternative<std::monostate>(values_[key])) {
      return values_.at(key);
    }
    if (enclosing_env != nullptr) {
      return enclosing_env->get(name);
    }
    return std::unexpected{std::errc::invalid_argument};
  }

  Result<Literal> assign(const std::string_view name, const Literal& value) {
    if (const std::string key{name}; values_.contains(key)) {
      return values_[key] = value;
    }
    if (enclosing_env != nullptr) {
      return enclosing_env->assign(name, value);
    }
    return std::unexpected{std::errc::invalid_argument};
  }

private:
  std::unordered_map<std::string, Literal> values_;
  std::shared_ptr<Env> enclosing_env;
};
}

#endif //CHAKA_CPP_ROOT_SRC_CHAKA_ENV_H_
