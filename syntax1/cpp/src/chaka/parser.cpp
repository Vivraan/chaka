/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 09-08-2023.
//

#include "parser.h"

#include <format>
#include <optional>
#include <vector>

#include "error.h"
#include "stmt.h"

namespace chaka {
namespace parser {
std::vector<StmtOwningPtr> Parser::parse() noexcept {
  std::vector<StmtOwningPtr> statements;
  while (!is_at_end()) {
    if (auto parse_result = parse_declaration(); parse_result.has_value()) {
      statements.push_back(std::move(parse_result.value()));
    }
  }
  return statements;
}

Result<StmtOwningPtr> Parser::parse_declaration() noexcept {
  // declaration -> var_def | statement;
  // var_def -> IDENT "/" "@def" ":" expression ";";
  if (check(TokenKind::Ident)) {
    const Token name = peek();
    advance();
    if (
      const auto peekf_result = peekf();
      check(TokenKind::Slash) && peekf_result.has_value() && peekf_result.value().lexeme == "@def"
      ) {
      advance();
      advance();
      auto parse_result = parse_var_def(name);
      if (!parse_result.has_value()) {
        synchronize();
      }
      return parse_result;
    }
  }
  auto parse_result = parse_statement();
  if (!parse_result.has_value()) {
    synchronize();
  }
  return parse_result;
}

Result<StmtOwningPtr> Parser::parse_var_def(const Token& name) noexcept {
  // var_def -> IDENT "/" "@def" ":" expression ";";
  if (
    auto consume_result = consume(TokenKind::Colon, "Expecting ':' after '/@def!'");
    !consume_result.has_value()
    ) {
    return std::unexpected{consume_result.error()};
  }
  auto init_result = parse_expression();
  if (!init_result.has_value()) {
    return std::unexpected{init_result.error()};
  }
  if (
    auto consume_result = consume(TokenKind::Semicolon, "Expecting ';' after the expression!");
    !consume_result.has_value()
    ) {
    return std::unexpected{consume_result.error()};
  }
  return stmt::make_var_def(name, std::move(init_result.value()));
}

Result<StmtOwningPtr> Parser::parse_statement() noexcept {
  // statement -> expression_statement | print_statement;
  if (check(TokenKind::Keyword)) {
    if (peek().lexeme == "@log") {
      return parse_dbg_log_statement();
    }
    if (peek().lexeme == "@$") {
      return parse_expression_statement();
    }
  } else if (advance_if_match({TokenKind::LBrace})) {
    if (auto parse_result = parse_block(); parse_result.has_value()) {
      return make_block(std::move(parse_result.value()));
    } else {
      return std::unexpected{parse_result.error()};
    }
  }
  std::unreachable();
}

Result<StmtOwningPtr> Parser::parse_expression_statement() noexcept {
  // expression_statement -> "@$" primary_expression ";";
  auto expr_result = parse_expression();
  if (!expr_result.has_value()) {
    return std::unexpected{expr_result.error()};
  }
  if (
    auto consume_result = consume(TokenKind::Semicolon, "Expecting ';' after the expression!");
    !consume_result.has_value()
    ) {
    return std::unexpected{consume_result.error()};
  }
  return stmt::make_expression(std::move(expr_result.value()));
}

Result<std::vector<StmtOwningPtr>> Parser::parse_block() noexcept {
  std::vector<StmtOwningPtr> statements;
  while (!check(TokenKind::RBrace) && !is_at_end()) {
    if (auto parse_decl_result = parse_declaration(); parse_decl_result.has_value()) {
      statements.push_back(std::move(parse_decl_result.value()));
    } else {
      return std::unexpected{parse_decl_result.error()};
    }
  }
  if (
    auto consume_rbrace_result = consume(TokenKind::RBrace, "Expecting '}' after block!");
    !consume_rbrace_result.has_value()
    ) {
    return std::unexpected{consume_rbrace_result.error()};
  }
  return statements;
}

Result<StmtOwningPtr> Parser::parse_dbg_log_statement() noexcept {
  // print_statement -> "@log" primary_expression ";";
  if (
    auto consume_result = consume_lexeme(TokenKind::Keyword, "@log", "Expecting '@log(...)'!");
    !consume_result.has_value()
    ) {
    return std::unexpected{consume_result.error()};
  }
  auto expr_result = parse_primary();
  if (!expr_result.has_value()) {
    return std::unexpected{expr_result.error()};
  }
  if (
    auto consume_result = consume(TokenKind::Semicolon, "Expecting ';' after the value!");
    !consume_result.has_value()
    ) {
    return std::unexpected{consume_result.error()};
  }
  return stmt::make_dbg_log(std::move(expr_result.value()));
}

Result<ExprOwningPtr> Parser::parse_expression() noexcept {
  if (
    auto consume_result = consume_lexeme(TokenKind::Keyword, "@$", "Expecting '@$(...)' to contain expression!");
    !consume_result.has_value()
    ) {
    return std::unexpected{consume_result.error()};
  }
  return parse_primary();
}

Result<ExprOwningPtr> Parser::parse_expression_internal() noexcept {
  // expression -> assignment;
  return parse_assignment();
}

Result<ExprOwningPtr> Parser::parse_assignment() noexcept {
  // assignment -> bit_and ( ":" assignment )?;
  auto parse_inner = parse_bit_and();
  if (!parse_inner.has_value()) {
    return parse_inner;
  }
  auto expr = std::move(parse_inner.value());
  if (advance_if_match({TokenKind::Colon})) {
    const auto peekb_result = peekb();
    if (!peekb_result.has_value()) {
      return std::unexpected{std::errc::invalid_argument};
    }
    auto parse_assign_result = parse_assignment();
    if (!parse_assign_result.has_value()) {
      return parse_assign_result;
    }
    auto eq = peekb_result.value();
    auto value = std::move(parse_assign_result.value());
    if (std::holds_alternative<expr::Variable>(*expr)) {
      Token name = std::get<expr::Variable>(*expr).name;
      return make_assignment(name, std::move(value));
    }
    return std::unexpected{blame(eq, "Invalid assignment target!")};
  }
  return expr;
}

// We use the precedence climbing method to parse expressions.
// We also stick with the old tradition of using the C grammar as a reference for precedence.

Result<ExprOwningPtr> Parser::parse_bit_and() noexcept {
  // bit_and -> bit_xor (( "&" ) bit_xor )*;
  return parse_binary_r2l({TokenKind::Ampersand}, &Parser::parse_bit_xor);
}

Result<ExprOwningPtr> Parser::parse_bit_xor() noexcept {
  // bit_xor -> bit_or (( "^" ) bit_or )*;
  return parse_binary_r2l({TokenKind::Caret}, &Parser::parse_bit_or);
}

Result<ExprOwningPtr> Parser::parse_bit_or() noexcept {
  // bit_or -> equality (( "|" ) equality )*;
  return parse_binary_r2l({TokenKind::Pipe}, &Parser::parse_equality);
}

Result<ExprOwningPtr> Parser::parse_equality() noexcept {
  // equality -> comparison (( "!=" | "==" ) comparison )*;
  return parse_binary_r2l({TokenKind::BangEql, TokenKind::Eql2}, &Parser::parse_comparison);
}

Result<ExprOwningPtr> Parser::parse_comparison() noexcept {
  // comparison -> addition (( ">" | ">=" | "<" | "<=" ) addition )*;
  return parse_binary_r2l(
    {TokenKind::RAngle, TokenKind::RAngleEql, TokenKind::LAngle, TokenKind::LAngleEql}, &Parser::parse_addition);
}

Result<ExprOwningPtr> Parser::parse_addition() noexcept {
  // addition -> multiplication (( "-" | "+" ) multiplication )*;
  return parse_binary_r2l({TokenKind::Minus, TokenKind::Plus}, &Parser::parse_multiplication);
}

Result<ExprOwningPtr> Parser::parse_multiplication() noexcept {
  // multiplication -> unary (( "/" | "*" ) unary )*;
  return parse_binary_r2l({TokenKind::Slash, TokenKind::Star}, &Parser::parse_bit_shift);
}

Result<ExprOwningPtr> Parser::parse_bit_shift() noexcept {
  // bit_shift -> unary (( "<<" | ">>" ) unary )*;
  return parse_binary_r2l({TokenKind::RAngle2, TokenKind::LAngle2}, &Parser::parse_unary);
}

Result<ExprOwningPtr> Parser::parse_binary_r2l(
  const std::initializer_list<TokenKind> kinds, const ParseFunc parse_next_higher_prec) noexcept {
  // binary -> next_higher_prec (( matches_list ) next_higher_prec )*;

  auto base_result = (this->*parse_next_higher_prec)();
  if (!base_result.has_value()) {
    return base_result;
  }
  while (advance_if_match(kinds)) {
    // In a binary expression, the left operand should always exist, meaning the op token must always be valid.
    std::optional<Token> op;
    if (auto prev_token = peekb(); !prev_token.has_value()) {
      std::unreachable();
    } else {
      op = prev_token.value();
    }
    // Right recursion
    auto right_result = (this->*parse_next_higher_prec)();
    if (!right_result.has_value()) {
      return right_result;
    }
    auto base = std::move(base_result.value());
    auto right = std::move(right_result.value());
    // Modify the base result variant
    base_result = make_binary(std::move(base), op.value(), std::move(right));
  }
  return base_result;
}

Result<ExprOwningPtr> Parser::parse_unary() noexcept {
  // unary -> ( "!" | "-" | "+" ) unary | primary;

  if (advance_if_match(
    {TokenKind::Bang, TokenKind::Minus, TokenKind::Plus, TokenKind::Percent, TokenKind::Tilde}
  )) {
    // A unary operator must always exist.
    std::optional<Token> op;
    if (auto prev_token = peekb(); !prev_token.has_value()) {
      std::unreachable();
    } else {
      op = prev_token.value();
    }
    if (auto right_result = parse_unary(); !right_result.has_value()) {
      return right_result;
    } else {
      return make_unary(op.value(), std::move(right_result.value()));
    }
  }
  return parse_primary();
}

Result<ExprOwningPtr> Parser::parse_primary() noexcept {
  // primary -> FLOAT | INT | STRING | IDENT | "@F" | "@T" | "(" expression ")";

  // Chaka's true and false literals are "@T" and "@F". These are hardcoded.
  if (advance_if_match({TokenKind::Keyword})) {
    // advance_if_match usually performs a peek -- the resulting token which is guaranteed to exist if the current_ index is okay.
    if (auto matched_token = peekb(); !matched_token.has_value()) {
      std::unreachable();
    } else {
      auto token = matched_token.value();
      if (token.lexeme == "@F") {
        token.literal = token::Literal{false};
        return expr::make_literal(token);
      }
      if (token.lexeme == "@T") {
        token.literal = token::Literal{true};
        return expr::make_literal(token);
      }
    }
  }
  if (advance_if_match({TokenKind::FltLit, TokenKind::IntLit, TokenKind::StrLit})) {
    if (auto matched_token = peekb(); !matched_token.has_value()) {
      std::unreachable();
    } else {
      return expr::make_literal(matched_token.value());
    }
  }
  if (advance_if_match({TokenKind::Ident})) {
    return expr::make_variable(peekb().value());
  }
  if (advance_if_match({TokenKind::LParen})) {
    auto base_result = parse_expression_internal();
    // Propagate any existing error
    if (!base_result.has_value()) {
      return base_result;
    }
    if (auto consume_result = consume(TokenKind::RParen, "Expecting ')' after the expression!");
      !consume_result.has_value()) {
      return std::unexpected{consume_result.error()};
    }
    return make_grouping(std::move(base_result.value()));
  }
  return std::unexpected{blame(peek(), "Expecting an expression!")};
}

/******* Helpers *******/

void Parser::synchronize() noexcept {
  advance();
  while (!is_at_end()) {
    if (auto prev_token = peekb(); prev_token.has_value() && prev_token.value().kind._value == TokenKind::Semicolon) {
      return;
    }
    // @if (@F) {
    // ^
    if (peek().kind._value == TokenKind::Keyword) {
      return;
    }
    // x: 0
    //  ^
    // OR
    // x / @stack: 0
    // ^ ^ ~~~~~~
    // 0 2 ----> peekf
    if (auto next_token = peekf(); next_token.has_value()) {
      switch (next_token.value().kind) {
        case TokenKind::Colon:
        case TokenKind::Slash:
          return;
        default:
          break;
      }
    }
    advance();
  }
}

bool Parser::advance_if_match(const std::initializer_list<TokenKind>& kinds_to_match) noexcept {
  for (const TokenKind& kind : kinds_to_match) {
    if (check(kind)) {
      advance();
      return true;
    }
  }
  return false;
}

Result<std::optional<Token>> Parser::consume(const TokenKind kind, const std::string_view message) noexcept {
  if (check(kind)) {
    return advance();
  }
  return std::unexpected{blame(peek(), message)};
}

Result<std::optional<Token>> Parser::consume_lexeme(
  const TokenKind& kind, const std::string_view lexeme, const std::string_view message) noexcept {
  if (check(kind) && peek().lexeme == lexeme) {
    return advance();
  }
  return std::unexpected{blame(peek(), message)};
}

std::errc Parser::blame(Token token, const std::string_view msg) noexcept {
  error(
    ErrorKind::Static, ErrorFatality::Fatal,
    token.log_info, token.kind._value == TokenKind::Eof
                    ? std::format("at end:\n{}", msg)
                    : std::format("at '{}':\n{}", token.lexeme, msg)
  );
  return std::errc::operation_not_supported;
}

bool Parser::check(const TokenKind& token_id) const noexcept {
  return !is_at_end() && peek().kind._value == token_id;
}

std::optional<Token> Parser::advance() noexcept {
  if (!is_at_end()) {
    current_++;
  }
  return peekb();
}

bool Parser::is_at_end() const noexcept {
  return peek().kind._value == TokenKind::Eof;
}

Token Parser::peek() const noexcept {
  return tokens_[current_];
}

std::optional<Token> Parser::peekf() const noexcept {
  const auto next_index = current_ + 1;
  return next_index < tokens_.size() ? std::optional{tokens_[next_index]} : std::nullopt;
}

std::optional<Token> Parser::peekb() const noexcept {
  const auto prev_index = current_ - 1;
  // ReSharper disable once CppUnsignedZeroComparison
  return prev_index >= 0 ? std::optional{tokens_[prev_index]} : std::nullopt;
}
} // namespace parser
} // namespace chaka
