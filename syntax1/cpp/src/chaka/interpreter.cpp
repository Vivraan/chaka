/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 17-09-2023.
//

#include "interpreter.h"

#include <format>
#include <print>
#include <gsl/util>

#include "error.h"
#include "stmt.h"
#include "token.h"

namespace chaka {
// Can't use <=> because bool also implements it, and the result type is std::strong_ordering but not exactly three-way.
template<typename T, typename U>
concept StronglyOrderedPair = requires(T a, U b)
{
  { a < b } -> std::convertible_to<std::strong_ordering>;
  { a > b } -> std::convertible_to<std::strong_ordering>;
  { a <= b } -> std::convertible_to<std::strong_ordering>;
  { a >= b } -> std::convertible_to<std::strong_ordering>;
  { a == b } -> std::convertible_to<std::strong_ordering>;
  { a != b } -> std::convertible_to<std::strong_ordering>;
};

namespace interpreter {
using token::i64;
using token::f64;
using token::Literal;
using token::TokenKind;

static Result<Literal> eval(const ExprViewingPtr expr_tree) noexcept {
  if (!expr_tree) {
    // This is a syntax error, not a runtime error.
    return Literal{std::monostate{}};
  }
  return std::visit(
    []<typename ExprUndecayT>(ExprUndecayT&& expr) noexcept -> Result<Literal> {
      using ExprT = std::decay_t<ExprUndecayT>;
      if constexpr (std::is_same_v<ExprT, expr::Unary>) {
        auto right_result = eval(expr::view(expr.right));
        if (!right_result.has_value()) {
          return right_result;
        }
        auto right = right_result.value();
        auto op = expr.op;
        return std::visit([op]<typename RY>(RY&& rhs) -> Result<Literal> {
          using R = std::decay_t<RY>;
          if constexpr (std::is_same_v<R, bool>) {
            switch (op.kind) {
              // Truthiness my arse.
              case TokenKind::Bang:
                return Literal{!rhs};
              default:
                break;
            }
          } else if constexpr (std::is_same_v<R, i64>) {
            switch (op.kind) {
              case TokenKind::Tilde:
                return Literal{~rhs};
              default:
                break;
            }
          } else if constexpr (std::is_arithmetic_v<R>) {
            switch (op.kind) {
              case TokenKind::Minus:
                return Literal{-rhs};
              case TokenKind::Plus:
                return Literal{rhs};
              default:
                break;
            }
          } [[unlikely]]
          {
            error(ErrorKind::Runtime, ErrorFatality::Fatal, op.log_info, "Invalid operand for unary operator.");
            std::unreachable();
          }
          // TODO Oops! This is dynamic typing. Figure out how to make the logic static, please?
        }, right);
        // ======================================== End Unary
      } else if constexpr (std::is_same_v<ExprT, expr::Binary>) {
        auto left_result = eval(expr::view(expr.left));
        if (!left_result.has_value()) {
          return left_result;
        }
        auto right_result = eval(expr::view(expr.right));
        if (!right_result.has_value()) {
          return right_result;
        }
        auto left = left_result.value();
        auto right = right_result.value();
        auto op = expr.op;
        return std::visit([op]<typename LX, typename RX>(LX&& lhs, RX&& rhs) -> Result<Literal> {
          using L = std::decay_t<LX>;
          using R = std::decay_t<RX>;
          // Mix and match.
          constexpr bool are_both_not_bools = !(std::is_same_v<L, bool> || std::is_same_v<R, bool>);
          constexpr bool are_both_int = std::is_same_v<L, i64> && std::is_same_v<R, i64>;
          constexpr bool are_both_arithmetic = std::is_arithmetic_v<L> && std::is_arithmetic_v<R>;
          // Note: this also works for booleans, since they are considered as integral types, but they promote to int.
          if constexpr (are_both_int && std::is_same_v<L, R>) {
            switch (op.kind) {
              case TokenKind::Caret:
                return Literal{lhs ^ rhs};
              case TokenKind::Ampersand:
                return Literal{lhs & rhs};
              case TokenKind::Pipe:
                return Literal{lhs | rhs};
              default:
                break;
            }
          }
          if constexpr (are_both_int && are_both_not_bools) {
            // Shifts are not for booleans
            switch (op.kind) {
              case TokenKind::LAngle2:
                return Literal{lhs << rhs};
              case TokenKind::RAngle2:
                return Literal{lhs >> rhs};
              default:
                break;
            }
          }
          // For some reason, booleans are considered as integral types, which are under the arithmetic type umbrella.
          // These obviously don't support arithmetic operations. TODO(vivraan) Or do they?
          if constexpr (are_both_arithmetic && are_both_not_bools) {
            switch (op.kind) {
              case TokenKind::Plus:
                return Literal{lhs + rhs};
              case TokenKind::Minus:
                return Literal{lhs - rhs};
              case TokenKind::Star:
                return Literal{lhs * rhs};
              case TokenKind::Slash: {
                if constexpr (std::is_same_v<R, f64>) {
                  if (std::fpclassify(rhs) != FP_ZERO) {
                    return Literal{lhs / rhs};
                  }
                } else if (rhs != 0) {
                  return Literal{lhs / rhs};
                }
                error(ErrorKind::Runtime, ErrorFatality::Fatal, op.log_info, "Division by zero.");
                return std::unexpected{std::errc::operation_not_supported};
              }
              default:
                break;
            }
          }
          if constexpr (
            // whhoooa boy... @formatter:off
						StronglyOrderedPair<L, R>
            // @formatter:on
          ) {
            switch (op.kind) {
              case TokenKind::LAngle:
                return lhs < rhs;
              case TokenKind::RAngle:
                return lhs > rhs;
              case TokenKind::LAngleEql:
                return lhs <= rhs;
              case TokenKind::RAngleEql:
                return lhs >= rhs;
              case TokenKind::Eql2:
                return lhs == rhs;
              case TokenKind::BangEql:
                return lhs != rhs;
              default:
                break;
            }
          } [[unlikely]]
          {
            error(ErrorKind::Runtime, ErrorFatality::Fatal, op.log_info, "Invalid operands for binary operator.");
            std::unreachable();
          }
        }, left, right);
        // ========================================= End Binary
      } else if constexpr (std::is_same_v<ExprT, expr::Grouping>) {
        return eval(expr::view(expr.inner));
      } else if constexpr (std::is_same_v<ExprT, Literal>) {
        return expr;
      } else if constexpr (std::is_same_v<ExprT, expr::Variable>) {
        return current_env->get(expr.name.lexeme);
      } else if constexpr (std::is_same_v<ExprT, expr::Assignment>) {
        if (auto eval_result = eval(expr::view(expr.value)); eval_result.has_value()) {
          return current_env->assign(expr.name.lexeme, eval_result.value());
        }
        error(ErrorKind::Runtime, ErrorFatality::Fatal, expr.name.log_info, "Invalid eval result.");
        // Evaluation may fail for any legitimate reason. Semantically, this may not be unreachable.
        std::terminate();
      } else {
        std::unreachable();
      }
    }, *expr_tree);
}

void interpret(const std::vector<StmtOwningPtr>& statements) noexcept {
  for (const auto& statement : statements) {
    execute(statement);
  }
}

void execute(const StmtOwningPtr& statement) noexcept {
  std::visit([]<typename StmtUndecayT>(StmtUndecayT&& stmt_var) noexcept {
    using StmtT = std::decay_t<StmtUndecayT>;
    if constexpr (std::is_same_v<StmtT, expr::ExprOwningPtr>) {
      if (auto eval_result = eval(expr::view(stmt_var)); eval_result.has_value()) {
        std::println("");
      } else {
        chaka::error(
          ErrorKind::Runtime,
          ErrorFatality::Fatal,
          stmt::get_log_info(stmt_var), std::make_error_condition(eval_result.error()).message()
        );
      }
    } else if constexpr (std::is_same_v<StmtT, stmt::DbgLog>) {
      if (auto eval_result = eval(expr::view(stmt_var.expr)); eval_result.has_value()) {
        std::println("{}", eval_result.value());
      } else {
        chaka::error(
          ErrorKind::Runtime,
          ErrorFatality::Fatal,
          expr::get_log_info(expr::view(stmt_var.expr)), std::make_error_condition(eval_result.error()).message()
        );
      }
    } else if constexpr (std::is_same_v<StmtT, stmt::VarDef>) {
      if (auto eval_result = eval(expr::view(stmt_var.initializer)); eval_result.has_value()) {
        current_env->define(std::string{stmt_var.name.lexeme}, eval_result.value());
        std::println("");
      } else {
        chaka::error(
          ErrorKind::Runtime,
          ErrorFatality::Fatal,
          stmt::get_log_info(stmt_var), std::make_error_condition(eval_result.error()).message()
        );
      }
    } else if constexpr (std::is_same_v<StmtT, stmt::Block>) {
      execute_block(stmt_var.statements, std::make_shared<Env>(current_env));
    } else {
      std::unreachable();
    }
  }, *statement);
}

void execute_block(const std::vector<StmtOwningPtr>& statements, const std::shared_ptr<Env>& env) noexcept {
  const auto prev_env = current_env;
  current_env = env;
  gsl::finally([&] { current_env = prev_env; });

  for (auto stmt : statements) {
    execute(stmt);
  }
}
} // namespace interpreter
} // namespace chaka
