/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 11-12-2023.
//

#include "stmt.h"

namespace chaka {
namespace stmt {
StmtOwningPtr make_dbg_log(ExprOwningPtr expr) noexcept {
  return std::make_unique<Stmt>(DbgLog{std::move(expr)});
}

StmtOwningPtr make_expression(ExprOwningPtr expr) noexcept {
  return std::make_unique<Stmt>(std::move(expr));
}

StmtOwningPtr make_var_def(Token name, ExprOwningPtr initializer) noexcept {
  return std::make_unique<Stmt>(VarDef{std::move(name), std::move(initializer)});
}

StmtOwningPtr make_block(std::vector<StmtOwningPtr>&& statements) noexcept {
  return std::make_unique<Stmt>(Block{std::move(statements)});
}

token::TokenLogInfo get_log_info(Stmt stmt) noexcept {
  return std::visit([]<typename StmtUndecayT>(StmtUndecayT&& stmt_var) -> token::TokenLogInfo {
    using StmtT = std::decay_t<StmtUndecayT>;
    if constexpr (std::is_same_v<StmtT, ExprOwningPtr>) {
      return expr::get_log_info(expr::view(stmt_var));
    } else if constexpr (std::is_same_v<StmtT, DbgLog>) {
      return expr::get_log_info(expr::view(stmt_var.expr));
    } else if constexpr (std::is_same_v<StmtT, VarDef>) {
      return stmt_var.name.log_info;
    } else if constexpr (std::is_same_v<StmtT, Block>) {
      return get_log_info(stmt::view(stmt_var.statements.front()));
    } else {
      std::unreachable();
    }
  }, stmt);
}
} // namespace stmt
} // namespace chaka
