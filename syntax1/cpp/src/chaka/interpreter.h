/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 17-09-2023.
//

#ifndef CHAKA_CPP_ROOT_SRC_CHAKA_INTERPRETER_H_
#define CHAKA_CPP_ROOT_SRC_CHAKA_INTERPRETER_H_

#include <expected>
#include <vector>

#include "env.h"
#include "error.h"
#include "expr.h"
#include "stmt.h"
#include "token.h"

namespace chaka::interpreter {

using token::Literal;
using expr::ExprViewingPtr;
using stmt::StmtOwningPtr;
using env::Env;

inline std::shared_ptr<Env> current_env;

static Result<Literal> eval(ExprViewingPtr expr_tree) noexcept;

void interpret(const std::vector<StmtOwningPtr>& statements) noexcept;

void execute(const StmtOwningPtr& statement) noexcept;

void execute_block(const std::vector<StmtOwningPtr>& statements, const std::shared_ptr<Env>& env) noexcept;

} // namespace chaka::interpreter


#endif //CHAKA_CPP_ROOT_SRC_CHAKA_INTERPRETER_H_
