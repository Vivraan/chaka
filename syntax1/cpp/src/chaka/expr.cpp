/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 08-07-2023.
//

#include "expr.h"

#include <format>

namespace chaka {
constexpr char GroupPh[] = "<group>";

namespace expr {

ExprOwningPtr make_binary(ExprOwningPtr left, const Token& op, ExprOwningPtr right) noexcept {
  return std::make_unique<Expr>(Binary{std::move(left), op, std::move(right)});
}

ExprOwningPtr make_grouping(ExprOwningPtr inner) noexcept {
  return std::make_unique<Expr>(Grouping{std::move(inner)});
}

ExprOwningPtr make_literal(const Token& literalToken) noexcept {
  return std::make_unique<Expr>(Literal{literalToken});
}

ExprOwningPtr make_unary(const Token& op, ExprOwningPtr right) noexcept {
  return std::make_unique<Expr>(Unary{op, std::move(right)});
}

ExprOwningPtr make_variable(const Token& name) noexcept {
  return std::make_unique<Expr>(Variable{name});
}

ExprOwningPtr make_assignment(const Token& name, ExprOwningPtr value) noexcept {
  return std::make_unique<Expr>(Assignment{name, std::move(value)});
}

token::TokenLogInfo get_log_info(ExprViewingPtr expr_tree_ptr) noexcept {
  return std::visit([]<typename ExprUndecayT>(ExprUndecayT&& expr) -> token::TokenLogInfo {
    using ExprT = std::decay_t<ExprUndecayT>;
    if constexpr (std::is_same_v<ExprT, Binary>) {
      return expr.op.log_info;
    } else if constexpr (std::is_same_v<ExprT, Grouping>) {
      return get_log_info(expr::view(expr.inner));
    } else if constexpr (std::is_same_v<ExprT, Literal>) {
      return expr.literalToken.log_info;
    } else if constexpr (std::is_same_v<ExprT, Unary>) {
      return expr.op.log_info;
    } else if constexpr (std::is_same_v<ExprT, Variable>) {
      return expr.name.log_info;
    } else {
      std::unreachable();
    }
  }, *expr_tree_ptr);
}

constexpr ExprViewingPtr view(const ExprOwningPtr& expr_owning_ptr) noexcept {
  return expr_owning_ptr.get();
}

std::string surround(
  const std::string_view name,
  const std::initializer_list<ExprViewingPtr> expr_viewing_ptrs,
  size_t num_groups
) noexcept {
  const auto first_expr_ptr = *expr_viewing_ptrs.begin();
  std::string subexprs_str = to_string(first_expr_ptr, num_groups);
  if (std::holds_alternative<Grouping>(*first_expr_ptr)) {
    ++num_groups;
  }
  for (auto it = std::next(expr_viewing_ptrs.begin()); it != expr_viewing_ptrs.end(); ++it) {
    const auto expr_ptr = *it;
    subexprs_str += ' ' + to_string(expr_ptr, num_groups);
    if (std::holds_alternative<Grouping>(*expr_ptr)) {
      ++num_groups;
    }
  }
  auto start_ws = name != GroupPh ? " " : '\n' + std::string(num_groups, ' ');
  auto end_ws = name != GroupPh ? "" : '\n' + std::string(num_groups - 1, ' ');
  return std::format("({}{}{}{})", name == GroupPh ? "" : name, start_ws, subexprs_str, end_ws);
}

std::string to_string(ExprViewingPtr expr_tree_ptr, const size_t num_groups) noexcept {
  thread_local auto ng = num_groups;
  if (!expr_tree_ptr) {
    return "<Empty expression node>";
  }
  return std::visit(
    [&]<typename ExprUndecayT>(ExprUndecayT&& expr) -> std::string {
      using ExprT = std::decay_t<ExprUndecayT>;
      if constexpr (std::is_same_v<ExprT, Binary>) {
        return surround(expr.op.lexeme, {view(expr.left), view(expr.right)}, ng);
      } else if constexpr (std::is_same_v<ExprT, Grouping>) {
        return surround(GroupPh, {view(expr.inner)}, ++ng);
      } else if constexpr (std::is_same_v<ExprT, Literal>) {
        return token::to_string(expr.literalToken.literal);
      } else if constexpr (std::is_same_v<ExprT, Unary>) {
        return surround(expr.op.lexeme, {view(expr.right)}, ng);
      } else if constexpr (std::is_same_v<ExprT, Variable>) {
        return surround(expr.name.lexeme, {}, ng);
      } else {
        return std::string{"<Unknown expression type>"};
      }
    }, *expr_tree_ptr);
}
} // namespace expr
} // namespace chaka
