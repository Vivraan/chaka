/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 08-07-2023.
//

#ifndef CHAKA_CPP_ROOT_SRC_EXPR_H_
#define CHAKA_CPP_ROOT_SRC_EXPR_H_

#include <variant>

#include "chaka/disc_union_concepts.h"
#include "token.h"

namespace chaka {
namespace expr {
using token::Token;

struct Binary;
struct Grouping;
struct Unary;
struct Variable;
struct Assignment;

struct Expr;
namespace {
using ExprOwningPtr = std::unique_ptr<const Expr>;
using ExprViewingPtr = const Expr* const;
}

struct Binary final {
  ExprOwningPtr left;
  Token op;
  ExprOwningPtr right;
};

struct Grouping final {
  ExprOwningPtr inner;
};

struct Literal final {
  Token literalToken;
};

struct Unary final {
  Token op;
  ExprOwningPtr right;
};

struct Variable final {
  Token name;
};

struct Assignment final {
  Token name;
  ExprOwningPtr value;
};

struct Expr : public std::variant<Binary, Grouping, Literal, Unary, Variable, Assignment> {
  using OwningPtr = ExprOwningPtr;
  using ViewingPtr = ExprViewingPtr;
};

/// @brief Alias for std::unique_ptr::get(). Should return a const pointer.
constexpr Expr::ViewingPtr view(const Expr::OwningPtr& expr_owning_ptr) noexcept;

static_assert(DiscUnionBasedNode<Expr, Binary, Grouping, Literal, Unary, Variable, Assignment>);

Expr::OwningPtr make_binary(Expr::OwningPtr left, const Token& op, Expr::OwningPtr right) noexcept;

Expr::OwningPtr make_grouping(Expr::OwningPtr inner) noexcept;

Expr::OwningPtr make_literal(const Token& literalToken) noexcept;

Expr::OwningPtr make_unary(const Token& op, Expr::OwningPtr right) noexcept;

Expr::OwningPtr make_variable(const Token& name) noexcept;

Expr::OwningPtr make_assignment(const Token& name, Expr::OwningPtr value) noexcept;

token::TokenLogInfo get_log_info(Expr::ViewingPtr expr_tree_ptr) noexcept;

std::string surround(
  std::string_view name,
  std::initializer_list<Expr::ViewingPtr> exprs,
  size_t num_groups
) noexcept;

std::string to_string(Expr::ViewingPtr expr_tree_ptr, size_t num_groups = 0) noexcept;
} // namespace expr
} // namespace chaka

#endif //CHAKA_CPP_ROOT_SRC_EXPR_H_
