/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2024. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 02-04-2024.
//

#ifndef CHAKA_CPP_ROOT_SRC_CHAKA_DISC_UNION_CONCEPTS_H_
#define CHAKA_CPP_ROOT_SRC_CHAKA_DISC_UNION_CONCEPTS_H_

#include <memory>
#include <variant>
#include "type_traits"

namespace chaka {

template<typename T, typename... Args>
concept DiscUnionBasedNode = std::is_base_of_v<std::variant<Args...>, T> && requires(T t) {
  typename T::OwningPtr;
  typename T::ViewingPtr;
  requires std::same_as<typename T::OwningPtr, std::unique_ptr<const T>>;
  requires std::convertible_to<typename T::ViewingPtr, const T* const>;
  { view(std::make_unique<T>(t)) } -> std::convertible_to<typename T::ViewingPtr>;
};

}

#endif //CHAKA_CPP_ROOT_SRC_CHAKA_DISC_UNION_CONCEPTS_H_
