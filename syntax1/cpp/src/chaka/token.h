/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 04-06-2023.
//

#ifndef CHAKA_SRC_TOKEN_H_
#define CHAKA_SRC_TOKEN_H_

#include <cstdint>
#include <ostream>
#include <variant>

#include "better-enums/enum.h"

namespace chaka {
namespace token {
BETTER_ENUM(
  TokenKind,
  uint32_t,
// Single-char tokens.
  LParen, // (
  RParen, // )
  LBrace, // {
  RBrace, // }
  LSquare, // [
  RSquare, // ]
  Comma, // ,
  Dot, // .
  Bang, // !
  Slash, // /
  Colon, // :
  Semicolon, // ;
  Plus, // +
  Minus, // -
  Star, // *
  Ampersand, // &
  Pipe, // |
  Caret, // ^
  LAngle, // <
  RAngle, // >
  Tilde, // ~
  Percent, // %
  QMark, // ?
// Two-char tokens.
  RArrow, // ->
  Eql2, // ==
  BangEql, // !=
  LAngleEql, // <=
  RAngleEql, // >=
  LAngle2, // <<
  RAngle2, // >>
// Multi-char tokens.
  Keyword,
  Ident, // [a-zA-Z_][a-zA-Z0-9_]*
  StrLit, // ".*"
  IntLit, // [0-9]+
  FltLit, // [0-9]+.[0-9]+
// Special IDs.
  Reserved, // Reserved for internal use.
  Eof // End of file.
)

struct TokenLogInfo final {
  size_t line;
  size_t column;
  size_t length;
};

using i64 = int64_t;
using f64 = double;
struct Literal : std::variant<std::monostate, std::string_view, i64, f64, bool> {};

struct Token final {
  Token(const TokenKind kind, const std::string_view lexeme, const Literal& literal, const TokenLogInfo& log_info) :
    kind(kind), lexeme(lexeme), literal(literal), log_info(log_info) {}

  friend std::string to_string(const Literal& lit_var);

  friend std::string to_string(const Token& token);

  friend std::ostream& operator<<(std::ostream& out, const Literal& literal);

  friend std::ostream& operator<<(std::ostream& out, const Token& token);

  TokenKind kind;
  std::string_view lexeme;
  Literal literal;
  TokenLogInfo log_info;
};

std::string to_string(const Literal& lit_var);

std::string to_string(const Token& token);

std::ostream& operator<<(std::ostream& out, const Literal& literal);

std::ostream& operator<<(std::ostream& out, const Token& token);
} // namespace token
} // namespace chaka

using chaka::token::i64;
using chaka::token::f64;
using chaka::token::Literal;

template<>
struct std::formatter<Literal> : std::formatter<std::string_view> {
  auto format(const Literal& lit_var, auto& ctx) const {
    return std::visit([&ctx]<typename T0>(T0&& literal) {
      using T = std::decay_t<T0>;
      if constexpr (std::is_same_v<T, std::monostate>) {
        return ctx.out();
      } else if constexpr (std::is_same_v<T, std::string_view>) {
        return std::format_to(ctx.out(), "Str({})", literal);
      } else if constexpr (std::is_same_v<T, i64>) {
        return std::format_to(ctx.out(), "Int({})", literal);
      } else if constexpr (std::is_same_v<T, f64>) {
        return std::format_to(ctx.out(), "Float({})", literal);
      } else if constexpr (std::is_same_v<T, bool>) {
        return std::format_to(ctx.out(), "Bool({})", literal ? "@T" : "@F");
      } else {
        std::unreachable();
      }
    }, lit_var);
  }
};

#endif //CHAKA_SRC_TOKEN_H_
