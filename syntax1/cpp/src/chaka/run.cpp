/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 + Copyright (c) 2023. Shivam "Vivraan" Mukherjee.                                                                    +
 +                                                                                                                    +
 + Permission is hereby granted, free of charge, to any person obtaining a                                            +
 + copy of this software and associated documentation files (the                                                      +
 + “Software”), to deal in the Software without restriction, including without                                        +
 + limitation the rights to use, copy, modify, merge, publish, distribute,                                            +
 + sublicense, and/or sell copies of the Software, and to permit persons to                                           +
 + whom the Software is furnished to do so, subject to the following                                                  +
 + conditions:                                                                                                        +
 +                                                                                                                    +
 + The above copyright notice and this permission notice shall be included                                            +
 + in all copies or substantial portions of the Software.                                                             +
 +                                                                                                                    +
 + THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY                                                          +
 + KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE                                                         +
 + WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR                                                            +
 + PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS                                                         +
 + OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR                                                           +
 + OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR                                                         +
 + OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE                                                          +
 + SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                             +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
// Created by vivraan on 26-07-2023.
//

#include "run.h"

#include <fstream>
#include <iostream>
#include <print>

#include "parser.h"
#include "ver.h"
#include "chaka/error.h"
#include "chaka/scanner.h"

namespace chaka {
std::string read_to_string(const std::filesystem::path& path) {
  std::ifstream ifs{path, std::ios::binary};
  std::string str(std::istreambuf_iterator{ifs}, {});
  return str;
}

void run_file(const std::filesystem::path& path) {
  try {
    const std::string source = read_to_string(path);
    run(source);
  } catch (const std::exception&) {
    std::println(stderr, "Failed to read file");
    // To match Rust's behaviour, we "panic" here.
    throw;
  }

  // Test fatality
  if (had_error.test(3)) {
    if (had_error[ErrorKind::Static]) {
      std::exit(65);
    }
    if (had_error[ErrorKind::Runtime]) {
      std::exit(70);
    }
  }
}

void run_prompt() noexcept {
  auto intro = std::format(
    "Chaka {}.{}.{}/C++ (C) Shivam \"Vivraan\" Mukherjee",
    CHAKA_VERSION_MAJOR,
    CHAKA_VERSION_MINOR,
    CHAKA_VERSION_PATCH);
  std::string bar(intro.length(), '=');
  std::print("{}\n{}\n", intro, bar);

  auto allowed_empty_prompts = 1;
  while (true) {
    std::print("> ");
    std::string line;
    std::getline(std::cin, line);

    if (line.empty()) {
      allowed_empty_prompts--;
      if (allowed_empty_prompts <= 0) {
        break;
      }
    } else {
      allowed_empty_prompts = 1;
    }
    run(line);
    had_error.reset();
  }
}

void run(const std::string_view source) noexcept {
  scanner::Scanner scanner{source};
  const auto tokens = scanner.scan_tokens();

  parser::Parser parser{tokens};
  const auto statements = std::move(parser.parse());

  if (had_error[ErrorKind::Static]) {
    return;
  }

  interpreter::interpret(statements);
}
}
