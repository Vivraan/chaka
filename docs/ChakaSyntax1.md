# Chaka Syntax 1

All things are `/@def`'d into existence. Future should allow specifying the memory segment for the variable (`/@stack` etc.), the allocation strategy (`/@alloc(ator)`), or ignoring it altogether by having a metaparser insert it into the token stream or parse tree automatically.

> For Chaka, _Future_ is a benevolent deity that confers upon us glimpses of the destiny of this language. 
> 
> Future withholds most information from us, so we can't know for sure if we'll defeat Rust or not. Future knows that we must continue developing Chaka in order to find out.
> 
> Maybe Future's not so benevolent after all.

Chaka is strongly typed. Its initial implementation will be dynamically typed, but Future says it will be statically typed.

## Scopes

Writing scopes is the mechanism for naming pieces of logic in Chaka. This allows class-like OOP.
```ka
// A scope returning a variable.
x/@def: @scope(tt) -> gg {
  // `gg` is a variable that lives in the application's stack memory segment
  gg/@def: 69;
  bb/@def: 420;

  gg: add(gg, bb);
  // add_set(%gg, bb); which takes `gg` as an in-out parameter.

  gg: mul(gg, tt);
  // mul_set(%gg, tt);
}

val/@def: x().gg;
print(val);
```

Idiomatically, a single return value will be named, or left as `@result` inside the body of the scope, which is a keyword.

`@result` can't be inside a scope when the public interface has one or more named members. It can be omitted in scopes which don't return anything.
```ka
make_hot_tea: @scope() {
  @result: @scope() -> temp_celsius {
    temp_celsius/@def: 40;
  }
}

tea: make_hot_tea();
// which is like tea: make_hot_tea().@result;

print(tea);
```

`@result` is only valid for the outermost scope. For nested scopes, use named members, as the `@result` variable will automatically be captured from the parent scope.

> This means that scopes have a clear distinction between semantics as a function and semantics as a type, even though they can be freely mixed syntactically.

Use bang to return the first public interface member by index.
```ka
print(tea()!0);
```

Although here it is better to name the member:
```ka
print(tea().temp_celsius);
```

A potential code smell is to have too many bang-index member accesses together:
```ka
print(tea()!0!1!0!3!69); // WHAT DO YOU WANT?!
```

So the compiler will flat-out warn if more than two appear at once. Of course, this opens up the possibility of minification.

Using `print` on a scope will print the details of its public interface, or just print the value of the `@result` member.

Chaka doesn't allow scope shadowing for variables. Using the same name again simply redefines the variable at its original scope.

---

**Q. Why would one name the only public interface member of a scope instead of just relying on `@result`?**

A. One good reason could be to meaningfully mark a scope which does not behave like a procedure/function/method in other languages for compatibility with C ABI (specifically the `__cdecl` calling convention), such as a single-member `struct`, which behaves differently in other languages.

The other reason is that implicitly, using `@result` means that calling the scope will not return the scope itself, but its only public interface member, so a scope can be optimised out if it is not needed. This may be optional for single-member scopes.

Then there are `@while` loops. Check the control flow section.

---

Let's try these out together to make a `Vec3`:
```ka
// A scope returning many variables and another scope, together called its public interface.
Vec3/@def: @scope() -> x, y, z, mag {
  // Vectorised assignment, future.
  [x, y, z]/@def: 0;

  // Constructor overload. Inherits the public interface of the parent scope.
  // This is perhaps the only way Chaka allows subtype inheritance.
  // This construct can't have zero arguments.
  @make(xx, yy, zz) {
    x: xx; y: yy; z: zz;
  }

  // Future should allow:
  // @make(x, y, z); this will fill the values of the shadowed public interface.

  // A nested scope.
  mag/@def: @scope() -> r {
    r/@def: @$(sqrt(x*x + y*y + z*z))
    
    // Future should allow: 
    //  r: sqrt(add(sq(x), sq(y), sq(z)));
    // The mandatory variable storage spec need will be relaxed in the future.
  }
}
```

The `@$` keyword allows the usage of regular scope calls with familiar arithmetic and logical operators.

---

**Q. Why go through the complexity of adding `@$` to do what all modern languages just give users automatically?**

A. This is to teach:
1. Myself the techniques of separating out this functionality at the language level.
2. Users by giving them the chance to ponder about what high-level constructs they are used to working with as well as how these adapt around the defaults of the language.

The `@$` construct is also an example of macro-like features for extending Chaka. They are like scopes in that they define some logic in curly braces. The difference lies in what syntax is permitted inside these blocks. These are restricted to single expressions, or are often delimiters for custom literals.

---

Back to the example:
```ka
v/@def: Vec3();

// Access variables returned by the scope.
print("(", v.x, ",", v.y, ",", v.z, ")");

// Access nested scope which uses variables of the parent scope, public or private.
// This call should print the public interface of `mag` as a pretty string.
print(v.mag());

// Use bang-index to return the first public interface member.
print(v.mag()!0);
// print(v.mag().r);
// Maybe you should go for `@result` here... see above.

// Create a Vec3 with arguments.
v: Vec3(1, 1, 1);
// v: Vec3().@make(1, 1, 1);
```

## Control Flow

Chaka's control flow is like C's, with `@if` and `@while`. Future says that pattern-matching and container iteration constructs are possible to implement, but must fit into the language cohesively.

One key difference is that both `@if` and `@while` support generator-like behaviour through the `/@yield` attribute-keyword as special scopes.

`/@yield`, like `/@def`, changes the behaviour of the call stack. Defining `/@yield` on a variable causes all variables in the public interface to be yielded/returned for one iteration (for if-scopes and even regular scopes, exactly one iteration). If the variable is defined in the outer scope, it acts as a mechanism to exit the outer scope early.

> `@result` can be yielded. Unlike other languages, returning and yielding are the same thing in Chaka.

```ka
// Chaka marks its keywords with @, so T and F are the way True and False are written.
@if (@T) {
  print("true");
} @elif (@F) {
  print("false");
} @else {
  print("huh??"); // It might take a while for any tooling I make to figure out that this is impossible.
}

// @if with init
// ? is the inline statement separator.
@if (i/@def: 0 ? i < 10) {
  print(i);
}

// Infinite while loop.
@while (@T) {
  print("true");
}

// Use the while-scope as a generator.
// This scope yields its value to the caller, and then continues from where it left off.
sq_x/@def: @while(i/@def: 0 ? i < 10) -> x {
  print(i);
  x/@yield: sq(i);
  inc(%i); // pre-increment i by 1; post-increment is not implemented. 
}
```

This runs after the loop is entered again. Control resumes at the beginning of the scope as though `inc(%i)` were called before `print(i)`.

`x` is auto yielded at the end of the scope if it is not yielded previously, like you would expect a scope to normally do with its public interface variables.

```ka
// next() is a special scope call which runs the while-scope until the next yield.
x/@def: sq_x.next();
// x/@def: sq_x().x;
sq_x.do(); // runs the rest of the loop -- future should allow iterating over generated values as a container.
```

Notice how we don't use `@$` for the condition in `@if` and `@while`. That's shorthand!

Breaking and skipping in loops happen differently. In either case, the loops need to be named.

1. To break the loop, call `break()` on the named loop:

      ```ka
      breaky/@def: @while(i/@def: 0 ? i < 10) {
         @if (i > 5) {
            breaky.break();
         }
      }
      ```
   
2. For continuing, it's `skip()` instead:

      ```ka
      breaky/@def: @while(i/@def: 0 ? i < 10) {
         @if (i == 5) {
            breaky.skip();
         }
      }
      ```
Named while-scopes can be used to break or skip in nested loop structures:

```ka
outer/@def: @while(i/@def: 0 ? i < 10) {
  inner/@def: @while(j/@def: 0 ? j < 10) {
    @if (i == j) {
      outer.skip(); // skip the entire outer loop execution
    }
  }
  inner.do(); // As they're named, they have to be called.
}
outer.do();
```

> Scopes do end up breaking some manner of hygiene, but that's anyways the case for skipping iterations in loops. However, it's a bit more general in Chaka. Maybe it will give people flashbacks of JavaScript. Maybe it won't. I don't know yet.


By default, the while scope will yield all of the following, if defined:
1. The initialised variables before the `?` used inside the loop.
1. The public interface variables.

---

#### Immediate Scopes

You must have noticed by now that scopes can be defined without a name, like the special while-scope. In this scenario, the scope is executed immediately and its public interface is returned. It's as if the scope got inlined. It doesn't make sense for default `@scope`s unless one has to manage the lifetimes of inner variables.

```ka
// In this scope, tt is a parameter and x is a public interface variable.
@scope(tt) -> x {
  x/@def: 69;
  bb/@def: 420;

  x: add(x, bb);
  // add_set(%x, bb);

  x: mul(x, tt);
  // mul_set(%x, tt);
  
  // bb's life ends here. A sad fate.
  // x is "returned". It's like saying it's raptured to the outer scope.
}
```

For a while loop, it's like defining a scope which is repeated as long as the condition holds. In this form, returning the public interface is not necessary, but it can be done, albeit without the advantage of holding it in a variable.

```ka
// This is a while scope which returns its public interface.
// It will stop after the last time it yields.
@while(i/@def: 0 ? i < 10) -> x {
  print(i);
  x/@yield: sq(i);
  inc(%i);
}
```

> I am really not sure if that `x` should be accessible ever. Maybe when file-scopes are a thing... maybe.

Even the file implicitly is an immediate scope. It's like a scope which is executed once and then its public interface is returned.

All scopes, immediate or not, capture their parent scope's variables.

> Future says that the module system will be implemented with file-scopes as the building blocks.

---

The `@result` keyword inside a while-scope will always refer to its parent scope's `@result` variable. A global while-scope cannot have a `@result` variable.

## Error Handling / TODO

Chaka will use multiple returns and eventually type unions to signal error states. This is a feature yet to be implemented.

```ka
// Future should allow:
@scope(to_validate) -> @U(x, err) {
  // @U() is a type union but without explicit or dynamic typing. 
  // It is different from an alias definition.
  @if (some_validator(to_validate)) {
    x/@udef: to_validate;
  } @else {
    err/@udef: Error("invalid input", 500);
  }
}
```
