# Chaka | চাকা 🛞

## Contents

* [Acknowledgements](#acknowledgements)
* [Preamble](#preamble)
  * [Why make Chaka?](#why-make-_chaka_)
  * [Chaka is NOT Made for Everything (necessarily)](#chaka-is-not-made-for-everything-necessarily)
  * [Chaka is Made for Everyone (iteratively)](#chaka-is-made-for-everyone-iteratively)
* [Milestones](#milestones)
  * [Chaka Syntax 1](#chaka-syntax-1)
  * [Lox implementation](#lox-implementation)
  * [Chaka Syntax 2 and transpilers](#chaka-syntax-2-and-transpilers)
  * [Consider more implementations for learning new languages](#consider-more-implementations-for-learning-new-languages)
    
---

## Acknowledgements

This project is based on [_Crafting Interpreters_](https://craftinginterpreters.com/) by Robert Nystrom (@munificent on Mastodon), available for free on the web and relatively affordably in print and ebook formats.

---

## Preamble

### Why make _Chaka_?

_Chaka_ (Bengali: চাকা, IPA: _cɑkɑ_) means "wheel", which is directly taken from the phrase "reinventing the wheel".

The explicit ethos of the language is to _reinvent_: it does the same stuff but there's a twist or ten.

The implicit ethos is to _open access_: we drop from the more formal/Sanskrit-leaning চক্র (IPA: _cɔːkro_) to symbolise that we found a way to refer to the same thing without the extra trills. _Tadbhramsa_ FTW.

<blockquote>
Notes(@vivraan):<br/><br/>

<details>
<summary>Opinions on Sanskrit [<a href="mailto:chaka-repo-corrections@vivraan.slmail.me">corrections welcome!</a>]</summary>
Note(@vivraan): Sanskrit is considered a sacred language and in history as one of status, in the domain of the higher castes and notably the Brahmin priesthood. It was not available to the commons, alienating the non-elite.

As an urban middle-class kid, I studied it in middle school.
</details>

I thought writing Chaka would be a good way to learn what happens inside programming languages, and as a learning process for in-depth understanding of more complex languages, such as Rust.

Well, then I got buffeted by life and responsibilities. I had to make a choice regarding Chaka, and the rest follows.
</blockquote>


### Chaka is NOT Made for Everything (necessarily).

We must acknowledge what limits Chaka, but also how it can evolve despite these limits.

1. **Chaka is NOT Made for Production Use.**
   
    [No warranties](LICENSE.txt) can be made about its reliability and stability.
   
   * _However_, it _could_ be [transpiled](#transpile-to-risc-v-assembly-language-tbd) and used through toolchains deployed in production.
   
      Its maintainers _may_ make guarantees about how a specific version of Chaka works, and whether it adheres to some compatibility process.
   
      > Note(@vivraan): Until we have a stable syntax, I make no guarantees. 🫠
      >  
      > Check out [Chaka Syntax 1](docs/ChakaSyntax1.md) for a first attempt.
    
1. **Chaka is NOT Made for High Performance Computing.**
   
   Chaka doesn't need features to squeeze as much performance out of your machine as it can; there are better languages and toolchains for doing that.

   * _However_, it _should_ allow tools processing its scripts to optimally use computing resources. No point in a wheel that can't turn.
    
1. **Chaka is NOT Made for General Purpose Programming.**

   Chaka cannot incorporate all the paradigms out there, not least because it'd be nigh impossible to complete the project.
   
   * _However_, it _could_ evolve towards one programming paradigm or another.
    
1. **Chaka is NOT Made for replacing existing software toolchains and pipelines.**

   Chaka, like the thousands of programming language projects out there, isn't replacing a core business system running COBOL any time soon, and is not designed for that purpose.
    
   * _However_, it _should_ learn from all software and change what doesn't work for existing systems within itself.
    
1. **Chaka is NOT Made to last.**

   Nor _can_ anything else. There may be multiple syntax specs, and they may all stop receiving support.


### Chaka is Made for Everyone (iteratively).

We must strive to make Chaka open to all people, but acknowledge that it will take time.

1. **Chaka must be made for all ages and abilities.**
    * It must be eventually facilitate kids' computer programming education, helping them learn and grow coding skills.
      * _Initial restriction: It will initially rely on lexical programming interfaces as opposed to graphical ones like Scratch or MakeCode, which may reduce its feasibility towards this goal._
    * It must eventually comply with the industry standards for accessible software tools and remain open to future ability-agnostic HCI interfaces.
      * _Initial restriction: It will initially rely on lexical programming interfaces as opposed to other accessible media, which may reduce its feasibility towards this goal._
    
1. **Chaka must be affordable to people from as many socioeconomic backgrounds as possible.**
    * People are free to pay what they want for this software, including no cost at all.

      > Note(@vivraan): Donate 💸🫰 to my PayPal on [my website](https://vivraan.coolpage.biz).

    * Chaka must strive to work on as many consumer computing devices as possible.
      * _Restriction: this will only be accomplished if it can be transpiled correctly._

Finally, the development of Chaka's syntax(es) will loosely follow the journey of Bob Nystrom's [Lox](#lox-implementation), but is its own language. Lox has been designed by Bob Nystrom to teach others how to write interpreters. Chaka differs in that it is the object of this learning process, and will expand more as I write a compiler for it in the future™.

---

## Milestones

### Chaka Syntax 1 

_[(doc)](docs/ChakaSyntax1.md)_

* Status: `suspended as of this commit`
   > Reason(@vivraan): I entered almost terminal hiatus due to Rust's learning curve and design ethos getting in the way of completing the interpreter implementation.
💀

### [Lox](https://craftinginterpreters.com/the-lox-language.html) implementation.

* Status: `initiating as of this commit`
   > Reason(@vivraan): It's apparent I haven't really made much progress in understanding the fundamentals, so implementations of Robert Nystrom's toy programming language, Lox, will be provided in this repo until I am satisfied that I can begin work on [Chaka Syntax 2](docs/ChakaSyntax2.md). 

* Implementation languages, ordered by preference:
   1. **C++ `default`.**
   1. **Rust `paused as of this commit`.**
   1. **[Anything else?](#consider-more-implementations-for-learning-new-languages) `TBD`.**
   > Reason(@vivraan): Competing implementations are great for learning languages (and I kinda now know Rust? neat!) but restricts learning the core concepts of what really goes into developing one. I am predisposed to getting side-tracked, so I'd like that not to happen a lot. For my sake, I should focus on what I can work with reasonably fast – C++(>23).


### Chaka Syntax 2 and transpilers.

_[(doc)](docs/ChakaSyntax2.md)_

* Status: `TBD`
    > Reason(@vivraan): RISC-V and WASM seem to be a fairly useful abstractions over machine instructions that serve as simple enough starting points for further work. Normally, I would've chosen LLVM or MLIR, but these machine instruction formats are quite useful in their own right.

### Consider more implementations for learning new languages.
   > Note(@vivraan): Zig, Elixir, OCaml, Mojo 🔥, etc. all seem quite interesting, not least due to their antiquity, novelty, or both. However, I must first implement Lox.

---
<footer style="text-align: center;">
Made with 💝 from 🇮🇳 and 🇬🇧 by <a href="https://vivraan.coolpage.biz">Vivraan</a> under the <a href="">MIT Licence</a>.
</footer>
